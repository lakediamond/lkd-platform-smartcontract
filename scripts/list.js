const utils = require("../utils.js");

const LakeDiamond = artifacts.require("./LakeDiamond.sol");
const OrderStorageAdapter = artifacts.require("./OrderStorageAdapter.sol");
const ProposalStorageAdapter = artifacts.require("./ProposalStorageAdapter.sol");
const TestToken = artifacts.require("./TestToken.sol");

const { deployedConfigPathConsts, getNetworkDeployedConfig } = require("../deployedConfigHelper");
const { getValueByPath } = require("../commonLogic");

module.exports = async function(callback) {
    const networkId = await utils.getNetworkId(web3);

    let deployedConfig = getNetworkDeployedConfig(networkId);

    try {
        let lakeDiamondInstanceAddress = getValueByPath(
            deployedConfig,
            deployedConfigPathConsts.deployedContracts.lakeDiamond.address.path
        );

        console.log(lakeDiamondInstanceAddress);

        LakeDiamondInstance = await LakeDiamond.at(lakeDiamondInstanceAddress);

        ProposalStorageAdapterInstance = await ProposalStorageAdapter.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.proposalStorageAdapter.address.path
            )
        );

        OrderStorageAdapterInstance = await OrderStorageAdapter.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.orderStorageAdapter.address.path
            )
        );

        TestTokenInstance = await TestToken.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.token.address.path
            )
        );

    } catch (e) {
        console.log(e);
        assert.ok(false);
    }


    owner = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.lakeDiamond.deployer.path);

    tokenOwner = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.token.deployer.path);
        
    subOwners = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.lakeDiamond.subOwners.path);
        
    backandAddress = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.lakeDiamond.backandAddress.path);
        
    console.log(
        `owner: "${owner}";\n subOwners: "${subOwners}";\n backandAddress: ` + 
        `"${backandAddress}";\n tokenOwner: "${tokenOwner}";\n `
    );


    let proposalId = await ProposalStorageAdapterInstance.getHeadId();
    console.log(proposalId.toString());
    let i = 0;

    try {
        
        while (proposalId.toString() != "0") {
            proposal = await ProposalStorageAdapterInstance.getProposal(proposalId);

            console.log(
                `index: ${i++}, id: ${proposalId.toString()}, price: ${proposal.price.toString()}, `
                + `token amount: ${proposal.tokenAmount}, owner: ${proposal.owner}, `
                + `beforeId: ${proposal.beforeId}, afterId: ${proposal.afterId}, `
            );

            proposalId = proposal.afterId.toString();
        }

    } catch (error) {
        console.log(error);
    }
    callback.call();
};

