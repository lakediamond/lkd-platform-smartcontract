const utils = require("../utils.js");

const LakeDiamond = artifacts.require("./LakeDiamond.sol");
const OrderStorageAdapter = artifacts.require("./OrderStorageAdapter.sol");
const ProposalStorageAdapter = artifacts.require("./ProposalStorageAdapter.sol");
const TestToken = artifacts.require("./TestToken.sol");

const { deployedConfigPathConsts, getNetworkDeployedConfig } = require("../deployedConfigHelper");
const { getValueByPath } = require("../commonLogic");

module.exports = async function(callback) {
    const networkId = await utils.getNetworkId(web3);

    let deployedConfig = getNetworkDeployedConfig(networkId);

    try {
        LakeDiamondInstance = await LakeDiamond.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.lakeDiamond.address.path
            )
        );

        ProposalStorageAdapterInstance = await ProposalStorageAdapter.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.proposalStorageAdapter.address.path
            )
        );

        OrderStorageAdapterInstance = await OrderStorageAdapter.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.orderStorageAdapter.address.path
            )
        );

        TestTokenInstance = await TestToken.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.token.address.path
            )
        );

    } catch (e) {
        console.log(e);
    }


    owner = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.lakeDiamond.deployer.path);

    tokenOwner = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.token.deployer.path);
        
    subOwners = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.lakeDiamond.subOwners.path);
        
    backandAddress = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.lakeDiamond.backandAddress.path);
        
    console.log(
        `owner: "${owner}";\nsubOwners: "${subOwners}";\nbackandAddress: ` + 
        `"${backandAddress}";\ntokenOwner: "${tokenOwner}";\n`
    );


    try {
        const printProposals = async () => {

            console.log("\nprint proposals list\n");
    
            let headId = await ProposalStorageAdapterInstance.getHeadId();
            let i = 0; 
            while (headId.toString() != "0") {
                let proposal = await ProposalStorageAdapterInstance.getProposal(headId);
        
                console.log(
                    `index: ${i++}, id: ${headId.toString()}, price: ${proposal.price.toString()}`
                    + `, token amount: ${proposal.tokenAmount}, beforeId: ${proposal.beforeId}, afterId: ${proposal.afterId}`
                );
        
                headId = proposal.afterId.toString();
            }
        };

        const proposalsOwner = tokenOwner;
        await TestTokenInstance.transfer(proposalsOwner, 10000000, { from: tokenOwner });
        await TestTokenInstance.approve(LakeDiamondInstance.address, 10000000, { from: proposalsOwner });
        await LakeDiamondInstance.addToWhitelist(proposalsOwner, 10000000, { from: backandAddress });
    
        const proposalTokenAmount = 100;


        var p1Id = await utils.createProposal(LakeDiamondInstance, 0.06, proposalTokenAmount, { from: proposalsOwner });
        console.log("p1 created");
        await LakeDiamondInstance.withdrawProposal(p1Id, { from: proposalsOwner });
        console.log("p1 withrawn");

        var p2Id = await utils.createProposal(LakeDiamondInstance, 0.06, proposalTokenAmount, { from: proposalsOwner });
        console.log("p2 created");
        await LakeDiamondInstance.withdrawProposal(p2Id, { from: proposalsOwner });
        console.log("p2 withrawn");

        const p3Price = 6;
        await LakeDiamondInstance.createProposal(p3Price, proposalTokenAmount, 0, { from: proposalsOwner });
        console.log(`p3 created with price ${p3Price}`);

        var p4Id = await utils.createProposal(LakeDiamondInstance, 0.06, proposalTokenAmount, { from: proposalsOwner });
        console.log("p4 created");
        await LakeDiamondInstance.withdrawProposal(p4Id, { from: proposalsOwner });
        console.log("p4 withrawn");

        var p5Id = await utils.createProposal(LakeDiamondInstance, 0.06, proposalTokenAmount, { from: proposalsOwner });
        console.log("p5 created");
        await LakeDiamondInstance.withdrawProposal(p5Id, { from: proposalsOwner });
        console.log("p5 withrawn");
        
        var p6Id = await utils.createProposal(LakeDiamondInstance, 0.06, proposalTokenAmount, { from: proposalsOwner });
        console.log("p6 created");
        await LakeDiamondInstance.withdrawProposal(p6Id, { from: proposalsOwner });
        console.log("p6 withrawn");
        
        const p7Price = 7;
        await LakeDiamondInstance.createProposal(p7Price, proposalTokenAmount, 0, { from: proposalsOwner });
        console.log(`p7 created with price ${p7Price}`);

        const p8Price = 4;
        await LakeDiamondInstance.createProposal(p8Price, proposalTokenAmount, 0, { from: proposalsOwner });
        console.log(`p8 created with price ${p8Price}`);

        await printProposals();

        await LakeDiamondInstance.setOrderType(1, "Test type");

        const orderPrice = 0.5;
        await utils.createOrder(LakeDiamondInstance, 1, "test", 0.5, proposalTokenAmount, 100.15);
        console.log(`created order with price ${orderPrice * 100} and token amount ${proposalTokenAmount}`);

        await printProposals();

        await utils.createOrder(LakeDiamondInstance, 1, "test", 0.5, proposalTokenAmount, 100.15);
        console.log(`created order with price ${orderPrice * 100} and token amount ${proposalTokenAmount}`);


        await printProposals();

        var p9Id = await utils.createProposal(LakeDiamondInstance, 0.06, proposalTokenAmount, { from: proposalsOwner });
        console.log("p9 created");
        await LakeDiamondInstance.withdrawProposal(p9Id, { from: proposalsOwner });
        console.log("p9 withrawn");

        const p10Price = 5;
        await LakeDiamondInstance.createProposal(p10Price, proposalTokenAmount, 0, { from: proposalsOwner });
        console.log(`p10 created with price ${p10Price}`);

        await printProposals();
        
    } catch (error) {
        console.log(error);
    }


    callback.call();
};

