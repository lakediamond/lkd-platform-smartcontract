const utils = require("../utils.js");

const LakeDiamond = artifacts.require("./LakeDiamond.sol");
const OrderStorageAdapter = artifacts.require("./OrderStorageAdapter.sol");
const ProposalStorageAdapter = artifacts.require("./ProposalStorageAdapter.sol");
const TestToken = artifacts.require("./TestToken.sol");

const { deployedConfigPathConsts, getNetworkDeployedConfig } = require("../deployedConfigHelper");
const { getValueByPath } = require("../commonLogic");

module.exports = async function(callback) {
    const networkId = await utils.getNetworkId(web3);

    let deployedConfig = getNetworkDeployedConfig(networkId);

    try {
        LakeDiamondInstance = await LakeDiamond.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.lakeDiamond.address.path
            )
        );

        ProposalStorageAdapterInstance = await ProposalStorageAdapter.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.proposalStorageAdapter.address.path
            )
        );

        OrderStorageAdapterInstance = await OrderStorageAdapter.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.orderStorageAdapter.address.path
            )
        );

        TestTokenInstance = await TestToken.at(
            getValueByPath(
                deployedConfig,
                deployedConfigPathConsts.deployedContracts.token.address.path
            )
        );

    } catch (e) {
        console.log(e);
        assert.ok(false);
    }


    owner = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.lakeDiamond.deployer.path);

    tokenOwner = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.token.deployer.path);
        
    subOwners = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.lakeDiamond.subOwners.path);
        
    backandAddress = getValueByPath(deployedConfig,
        deployedConfigPathConsts.deployedContracts.lakeDiamond.backandAddress.path);
        
    console.log(
        `owner: "${owner}";\nsubOwners: "${subOwners}";\nbackandAddress: ` + 
        `"${backandAddress}";\ntokenOwner: "${tokenOwner}";\n`
    );

    const proposalsCount = 2;

    try {
        const proposalsOwner = tokenOwner;
        //wait TestTokenInstance.transfer(proposalsOwner, 10000000, { from: tokenOwner });
        //wait TestTokenInstance.approve(LakeDiamondInstance.address, 10000000, { from: proposalsOwner });
        //wait LakeDiamondInstance.addToWhitelist(proposalsOwner, 10000000, { from: backandAddress });
    
        let maxGasConsumption = 0;
        let gasConsumptionAmount = 0;
    
        let createdProposals = [];
    
        const proposalTokenAmount = 400;
    
        let headId = 0;
        console.log(`creating ${proposalsCount} proposals`);
        for (let i = 0; i < proposalsCount; i++) {
            createdProposals = createdProposals.sort((a, b) => { return a.price - b.price });
    
            headId = await ProposalStorageAdapterInstance.getHeadId();
    
            const price = 3;
            
            let startIndex = createdProposals.filter((proposal) => { return proposal.price <= price }).length - 1
            const startId = startIndex >= 0 ? createdProposals[startIndex].id : 0;
            
            let result = await LakeDiamondInstance.createProposal(price, proposalTokenAmount, startId, { from: proposalsOwner });
            
            const createdProposalId = result.logs[1].args["id"];
            console.log(`new proposal id: ${createdProposalId}`);
    
            const createdProposal = {
                id: createdProposalId,
                price: price
            }
    
            createdProposals.push(createdProposal);
    
            maxGasConsumption = Math.max(maxGasConsumption, result.receipt.cumulativeGasUsed);
    
            if (result.receipt.cumulativeGasUsed > 500000) {
                console.log(`create proposal get a lot of gas. Gas consumption: ${result.receipt.cumulativeGasUsed}, `
                    + `proposal: ${JSON.stringify(createdProposal)}, startId: ${startId}`);
            }
    
            gasConsumptionAmount += result.receipt.cumulativeGasUsed;
        }
        console.log(`max gas consumption: ${maxGasConsumption}, avg gas comsumption: ${gasConsumptionAmount / proposalsCount}`);
        console.log("Proposals for gas consumption test created");
    
    
        let i = 0; 
        while (headId.toString() != "0") {
            let proposal = await ProposalStorageAdapterInstance.getProposal(headId);
    
            console.log(
                `index: ${i++}, id: ${headId.toString()}, price: ${proposal.price.toString()}`
                + `, token amount: ${proposal.tokenAmount}`
            );
    
            headId = proposal.afterId.toString();
        }
        
    } catch (error) {
        console.log(error);
    }


    callback.call();
};

