var LakeDiamond = artifacts.require("./LakeDiamond.sol");
var LakeDiamondStorage = artifacts.require("./LakeDiamondStorage.sol");
var OrderStorageAdapter = artifacts.require("./OrderStorageAdapter.sol");
var ProposalStorageAdapter = artifacts.require("./ProposalStorageAdapter.sol");
var TestToken = artifacts.require("./TestToken.sol");

const { deployedConfigPathConsts, saveDeployedConfig, getNetworkDeployedConfig } = require("../deployedConfigHelper");
const { getFormatedConsoleLabel, setValueByPath } = require("../commonLogic");
var utils = require("../utils.js");


module.exports = function(deployer, network, accounts) {
    console.log(getFormatedConsoleLabel("deploy all contracts: "));

    deployer.then(async () => {
        const networkId = await utils.getNetworkId(web3);
        let deployedConfig = getNetworkDeployedConfig(networkId);
        const contractPathConsts = deployedConfigPathConsts.deployedContracts;
        try {
            const mainDeployer = accounts[0];
            setValueByPath(deployedConfig, deployedConfigPathConsts.mainDeployerAddress.path, mainDeployer);
    
            await deployer.deploy(TestToken, { from: mainDeployer });
            console.log(`TestToken deployed at address ${TestToken.address}`);
            setValueByPath(deployedConfig, contractPathConsts.token.address.path, TestToken.address);
            setValueByPath(deployedConfig, contractPathConsts.token.deployer.path, mainDeployer);
    
            await deployer.deploy(LakeDiamondStorage, TestToken.address, { from: mainDeployer });
            console.log(`LakeDiamondStorage deployed at address ${LakeDiamondStorage.address}`);
            setValueByPath(deployedConfig, contractPathConsts.lakeDiamondStorage.address.path, LakeDiamondStorage.address);
            setValueByPath(deployedConfig, contractPathConsts.lakeDiamondStorage.deployer.path, mainDeployer);
    
            await deployer.deploy(OrderStorageAdapter, LakeDiamondStorage.address, { from: mainDeployer });
            console.log(`OrderStorageAdapter deployed at address ${OrderStorageAdapter.address}`);
            setValueByPath(deployedConfig, contractPathConsts.orderStorageAdapter.address.path, OrderStorageAdapter.address);
    
            await deployer.deploy(ProposalStorageAdapter, LakeDiamondStorage.address, { from: mainDeployer });
            console.log(`ProposalStorageAdapter deployed at address ${ProposalStorageAdapter.address}`);
            setValueByPath(deployedConfig, contractPathConsts.proposalStorageAdapter.address.path, ProposalStorageAdapter.address);
            
            const backandAddress = accounts.length > 1 ? accounts[1] : accounts[0];
            const lakeDiamondOwner = accounts.length > 2 ? accounts[2] : accounts[0];
            const minProposalTokenAmount = 100;
            console.log(`accounts: ${accounts}`);
            await deployer.deploy(LakeDiamond, LakeDiamondStorage.address, backandAddress, minProposalTokenAmount, { from: lakeDiamondOwner });
            console.log(`LakeDiamond deployed at address ${LakeDiamond.address}` + 
                ` from address: ${lakeDiamondOwner}, backandAddress: ${lakeDiamondOwner}, minProposalTokenAmount: ${minProposalTokenAmount}`);
            setValueByPath(deployedConfig, contractPathConsts.lakeDiamond.address.path, LakeDiamond.address);
            setValueByPath(deployedConfig, contractPathConsts.lakeDiamond.deployer.path, lakeDiamondOwner);
            setValueByPath(deployedConfig, contractPathConsts.lakeDiamond.backandAddress.path, backandAddress);
            setValueByPath(deployedConfig, contractPathConsts.lakeDiamond.minProposalTokenAmount.path, minProposalTokenAmount);
            
            const storage = await LakeDiamondStorage.deployed();
            
            await storage.setLakeDiamondAddress(LakeDiamond.address);
            console.log(`LakeDiamondStorage parameter lakeDiamondAddress = ${LakeDiamond.address}`);
            setValueByPath(deployedConfig, contractPathConsts.lakeDiamondStorage.lakeDiamondAddress.path, 
                LakeDiamond.address);
            
            await storage.setProposalStorageAdapterAddress(ProposalStorageAdapter.address);
            console.log(`LakeDiamondStorage parameter proposalStorageAdapterAddress = ${ProposalStorageAdapter.address}`);
            setValueByPath(deployedConfig, contractPathConsts.lakeDiamondStorage.proposalStorageAdapterAddress.path, 
                ProposalStorageAdapter.address);
            
            await storage.setOrderStorageAdapterAddress(OrderStorageAdapter.address);
            console.log(`LakeDiamondStorage parameter orderStorageAdapterAddress = ${OrderStorageAdapter.address}`);
            setValueByPath(deployedConfig, contractPathConsts.lakeDiamondStorage.orderStorageAdapterAddress.path, 
                OrderStorageAdapter.address);
            
            const lakeDiamond = await LakeDiamond.deployed();
            const subOwners = [mainDeployer];
            await lakeDiamond.addSubOwners(subOwners, { from: lakeDiamondOwner });
            console.log(`LakeDiamond subOwners: ${subOwners}`);
            setValueByPath(deployedConfig, contractPathConsts.lakeDiamond.subOwners.path, 
                subOwners);
        } catch (error) {
            console.error(error);
        }
        saveDeployedConfig();
    });
};
