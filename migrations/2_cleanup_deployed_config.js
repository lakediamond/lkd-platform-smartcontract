const { 
    saveDeployedConfig,
    cleanupDeployedConfig,
    getNetworkDeployedConfig,
    deployedConfigPathConsts } = require("../deployedConfigHelper");

const truffleConfig = require("../truffle");
var utils = require("../utils.js");

/**
 * cleanup of previous migration outputs, saving new one with initial seed data
 */
module.exports = function(deployer, network, accounts) {
    deployer.then(async () => {
        const networkId = await utils.getNetworkId(web3);

        cleanupDeployedConfig(networkId);
        let config = getNetworkDeployedConfig(networkId);
        
        config[deployedConfigPathConsts.mainDeployerAddress.path] = 
            (truffleConfig.networks[network] || {}).from || accounts[0];
        config.networkName = network;
        saveDeployedConfig();
    });
};
