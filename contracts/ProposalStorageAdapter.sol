pragma solidity 0.5.7;


import "./LakeDiamondStorage.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";

/**
 * @title Proposal storage and management 
 * @author Bohdan Hrytsenko
 * @notice Single point to manage proposals
 */
contract ProposalStorageAdapter {
    using SafeMath for uint;
    
    LakeDiamondStorage public lakeDiamondStorage;

    /// public fields ///

    bytes32 public constant enitityNameKey =        0xb6d2dc83590271a7c0a5ab5fbf6a2dad418bbfd533c253e3d69a6772712809c7; //keccak256("proposal");
    bytes32 public constant ownerColumnKey =        0x02016836a56b71f0d02689e69e326f4f4c1b9057164ef592671cf0d37c8040c0; //keccak256("owner");
    bytes32 public constant priceColumnKey =        0x282bd803c09c6b34a4d86ee95434129ea89232e91fab09f9e5dc6fe984fa9a6f; //keccak256("price");
    bytes32 public constant tokenAmountColumnKey =  0x32df381ac93a61bd1f91ecd61cbf2ca27712cdcb73f6a80b6af9b67ecb84ac78; //keccak256("tokenAmount");
    bytes32 public constant lastProposalIdKey =     0xa5b5d087bd01df05f8828446f34a933eec75f5ebb2683d9a643bec2d031a7190; //keccak256("lastProposalId");
    bytes32 public constant afterColumnKey =        0xf13f8f48f784339836074bca589b438c6e583d5a28900901e1cb2141728a9248; //keccak256("after");
    bytes32 public constant beforeColumnKey =       0x20db4bb3b2034696124254dcded9e30e13e0e87193c8349a1ec155a5031d4465; //keccak256("before");
    bytes32 public constant headKey =               0xe902518e4ac0226265aa06273e3bdb251bb6a1a99c51280e46cb131f09bd4927; //keccak256("proposalsHead");
    
    /// constructors ///

    constructor (LakeDiamondStorage lakeDiamondStorageContract) public {
        require(address(lakeDiamondStorageContract) != address(0), "lakeDiamondStorageContract argument can`t be empty");
        lakeDiamondStorage = lakeDiamondStorageContract;
    }

    /// modifiers ///

    modifier lakeDiamondContractOnly() {
        require(
            lakeDiamondStorage.lakeDiamondAddress() == msg.sender,
            "Only LakeDiamond contract can write data to storage"
        );
        _;
    }

    /// public methods ///

    /**
    @notice create proposal record in storage.
        Proposals stores as double linked list sorted by price. If price is equal, newest proposal will be last. 
    @param owner proposal owner, address
    @param price price per token in proposal, uint
    @param tokenAmount amount of tokens to sell, uint
    @param startPositionId id of proposal from which start to search position to insert
    @return id of created record
    */
    function createProposal(address payable owner, uint price, uint tokenAmount, uint startPositionId)
    external 
    lakeDiamondContractOnly()
    returns(uint256 proposalId) { 
        (uint256 beforeId, uint256 afterId) = findPosition(startPositionId, price);

        proposalId = lakeDiamondStorage.getUint(lastProposalIdKey) + 1;
        bytes32 baseKey = getBaseKey(proposalId);

        setOwner(baseKey, owner);
        setPrice(baseKey, price);
        setTokenAmount(baseKey, tokenAmount);
        setAfterId(baseKey, afterId);
        setBeforeId(baseKey, beforeId);
        
        lakeDiamondStorage.setUint(lastProposalIdKey, proposalId);

        if (beforeId > 0) {
            setAfterId(getBaseKey(beforeId), proposalId);
        } else {
            setHeadId(proposalId);
        }
        
        if (afterId > 0) {
            setBeforeId(getBaseKey(afterId), proposalId);
        }
    }

    /**
    @notice create proposal record in storage
    @param proposalId id of proposal
    @return {
        "owner": "proposal ovenr address",
        "price": "proposal price uint",
        "tokenAmount": "amount of tokens to sell in proposal, uint",
        "isActive": "is proposal active, bool"
    }   
    */
    function getProposal(uint proposalId) 
    public
    view
    returns(address payable owner, uint price, uint tokenAmount, uint afterId, uint beforeId) {
        uint lastProposalId = lakeDiamondStorage.getUint(lastProposalIdKey) + 1;
        require(lastProposalId >= proposalId, "Proposal not exists");

        bytes32 baseKey = getBaseKey(proposalId);

        owner = getOwner(baseKey);
        price = getPrice(baseKey);
        tokenAmount = getTokenAmount(baseKey);

        afterId = getAfterId(baseKey);
        beforeId = getBeforeId(baseKey);
    }
    
    function getProposalPrice(uint proposalId) public view returns(uint price) {
        uint lastProposalId = lakeDiamondStorage.getUint(lastProposalIdKey) + 1;
        require(lastProposalId >= proposalId, "Proposal not exists");

        bytes32 baseKey = getBaseKey(proposalId);

        price = getPrice(baseKey);
    }

    /**
    @notice withdraw proposal, set status to inactive
    @param proposalId id of proposal
    @return amount of remaining tokens
    */
    function withdrawProposal(uint proposalId) public lakeDiamondContractOnly() returns(uint proposalAmount) {
        bytes32 baseKey = getBaseKey(proposalId);

        proposalAmount = getTokenAmount(baseKey);
        require(proposalAmount > 0, "Proposal token amount must be greater than 0");
        setTokenAmount(baseKey, 0);

        uint beforeId = getBeforeId(baseKey);
        uint afterId = getAfterId(baseKey);

        if (afterId > 0) {
            setBeforeId(getBaseKey(afterId), beforeId);
        }
        if (beforeId > 0) {
            setAfterId(getBaseKey(beforeId), afterId);
        }
        
        if (getHeadId() == proposalId) {
            setHeadId(afterId);
        }
    }

    /**
    @notice proposal owner getter
    @param proposalId id of proposal
    @return proposal owner address
    */
    function getProposalOwner(uint proposalId) external view returns (address payable owner) {
        owner = getOwner(getBaseKey(proposalId));
    }

    /**
    @notice proposal price getter
    @param proposalId id of proposal
    @return proposal price value
    */
    function getPrice(uint proposalId) external view returns (uint price) {
        price = getPrice(getBaseKey(proposalId));
    }

    /**
    @notice proposal tokens amount getter
    @param proposalId id of proposal
    @return proposal tokens amount
    */
    function getTokenAmount(uint proposalId) external view returns (uint price) {
        price = getTokenAmount(getBaseKey(proposalId));
    }
    
    /**
    @notice set new proposal amount of tokens
    @param proposalId id of proposal
    @param newTokenAmount proposal tokens amount after completition
     */
    function setTokenAmount(uint proposalId, uint newTokenAmount)
    external
    lakeDiamondContractOnly() {
        bytes32 baseKey = getBaseKey(proposalId);

        setTokenAmount(baseKey, newTokenAmount);
    }   

    /**
    @notice proposal after id getter
    @param proposalId id of proposal
    @return next proposal id
    */
    function getAfterId(uint proposalId) public view returns(uint value) {
        value = getAfterId(getBaseKey(proposalId));
    }

    /**
    @notice set new head id for linked list
    @param newHeadId new head id
    */
    function setHeadId(uint newHeadId) public lakeDiamondContractOnly() {
        uint lastProposalId = lakeDiamondStorage.getUint(lastProposalIdKey);
        
        require(newHeadId <= lastProposalId, "New head id can`t be greater than last created proposal id");

        lakeDiamondStorage.setUint(headKey, newHeadId);
        if (newHeadId == 0) {
            return;
        }

        bytes32 baseKey = getBaseKey(newHeadId);
        require(getTokenAmount(baseKey) > 0, "Proposal inactive");

        uint newHeadBeforeId = getBeforeId(baseKey);
        
        if (newHeadBeforeId > 0) {
            setAfterId(getBaseKey(newHeadBeforeId), 0);
        }
        setBeforeId(baseKey, 0);
    }

    /**
    @notice get current head id of linked list
    @return uint
    */
    function getHeadId() public view returns(uint) {
        return lakeDiamondStorage.getUint(headKey);
    }

    /**
    @notice find probably proposal position in sorted list
    @param startPosition id of the proposal from which the search begins
    @param price new proposal price
    @return {
        "beforeId": "proposal id which must be before new proposal",
        "beforeId": "proposal id which must be after new proposal"
    }
    */
    function findPosition(uint startPosition, uint price) public view returns (uint beforeId, uint afterId) {
        uint headId = getHeadId();
        if (headId == 0) {
            return (0, 0); // List is empty. Current proposal will be first and head
        }
        uint currentId = startPosition > 0 ? startPosition : headId; // if start position is 0(hint not provided) use headId as start position

        bytes32 baseKey = getBaseKey(currentId);

        require(getTokenAmount(baseKey) > 0, "Proposal inactive or not exist");

        uint startPositionPrice = getPrice(baseKey);

        // if start position is head and new proposal price is lower than head price then new proposal will be new head
        if (price < startPositionPrice && currentId == headId) {  
            return (0, currentId);
        }

        require(price >= startPositionPrice, "Start position price must be greater or equal to new proposal price");

        uint previousId = currentId;

        while (true) { // start lookup for place to insert
            currentId = getAfterId(baseKey);
            if (currentId == 0) { // if next proposal not exists stop look up for place. New proposal will be tail
                break;
            }

            baseKey = getBaseKey(currentId);
            
            uint currentPrice = getPrice(baseKey);
            if (price < currentPrice) {
                return (previousId, currentId); // position found
            }

            previousId = currentId;
        }

        // new proposal is tail
        return (previousId, 0);
    }

    /// private methods ///

    function setAfterId(bytes32 baseKey, uint value) internal {
        bytes32 afterKey = keccak256(abi.encode(baseKey, afterColumnKey));
        lakeDiamondStorage.setUint(afterKey, value);
    }

    function setBeforeId(bytes32 baseKey, uint value) internal {
        bytes32 beforeKey = keccak256(abi.encode(baseKey, beforeColumnKey));
        lakeDiamondStorage.setUint(beforeKey, value);
    }

    function setOwner(bytes32 baseKey, address payable owner) private {
        bytes32 ownerKey = keccak256(abi.encode(baseKey, ownerColumnKey));
        lakeDiamondStorage.setPayableAddress(ownerKey, owner);
    }

    function setPrice(bytes32 baseKey, uint price) private {
        bytes32 priceKey = keccak256(abi.encode(baseKey, priceColumnKey));
        lakeDiamondStorage.setUint(priceKey, price);
    }

    function setTokenAmount(bytes32 baseKey, uint tokenAmount) private {
        bytes32 tokenAmountKey = keccak256(abi.encode(baseKey, tokenAmountColumnKey));
        lakeDiamondStorage.setUint(tokenAmountKey, tokenAmount);
    }

    function getOwner(bytes32 baseKey) private view returns(address payable) { 
        bytes32 ownerKey = keccak256(abi.encode(baseKey, ownerColumnKey));
        return lakeDiamondStorage.getPayableAddress(ownerKey);
    }
    
    function getPrice(bytes32 baseKey) private view returns(uint) { 
        bytes32 priceKey = keccak256(abi.encode(baseKey, priceColumnKey));
        return lakeDiamondStorage.getUint(priceKey);
    }

    function getTokenAmount(bytes32 baseKey) private view returns(uint) { 
        bytes32 tokenAmountKey = keccak256(abi.encode(baseKey, tokenAmountColumnKey));
        return lakeDiamondStorage.getUint(tokenAmountKey);
    }

    function getAfterId(bytes32 baseKey) internal view returns(uint value) {
        bytes32 afterKey = keccak256(abi.encode(baseKey, afterColumnKey));
        value = lakeDiamondStorage.getUint(afterKey);
    }

    function getBeforeId(bytes32 baseKey) internal view returns(uint value) {
        bytes32 beforeKey = keccak256(abi.encode(baseKey, beforeColumnKey));
        value = lakeDiamondStorage.getUint(beforeKey);
    }

    function getBaseKey(uint proposalId) private pure returns (bytes32 key) {
        key = keccak256(abi.encode(enitityNameKey, proposalId));
    }    
}