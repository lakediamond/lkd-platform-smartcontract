pragma solidity 0.5.7;


import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "./IBurnableERC20.sol";

contract TestToken is Ownable, IBurnableERC20 { //ERC - 20 token contract
    using SafeMath for uint;
    // Triggered when tokens are transferred.
    event Transfer(address indexed _from, address indexed _to, uint256 _value);

    // Triggered whenever approve(address _spender, uint256 _value) is called.
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);

    string public constant symbol = "LKD";
    string public constant name = "LAKEDIAMOND";
    uint8 public constant decimals = 0;
    uint256 private _totalSupply = 200000000; //include decimals;

    // Balances for each account
    mapping(address => uint256) balances;

    // Owner of account approves the transfer of an amount to another account
    mapping(address => mapping (address => uint256)) allowed;

    constructor() public {
        balances[owner()] = _totalSupply;
        emit Transfer(address(this), owner(), _totalSupply);
    }  

    function totalSupply() public view returns (uint256) { //standard ERC-20 function
        return _totalSupply;
    }

    function balanceOf(address owner) public view returns (uint256 balance) {//standard ERC-20 function
        return balances[owner];
    }


    //standard ERC-20 function
    function transfer(address to, uint256 amount) public returns (bool success) {
        require(to != address(0), "Cant send tokens to address(0)");
        require(address(this) != to, "Cant send tokens to token contract address");
        balances[msg.sender] = balances[msg.sender].sub(amount);
        balances[to] = balances[to].add(amount);
        emit Transfer(msg.sender, to, amount);
        return true;
    }

    //standard ERC-20 function
    function transferFrom(address from, address to, uint256 amount) public returns(bool success){
        require(to != address(0), "Cant send tokens to address(0)");
        require(address(this) != to, "Cant send tokens to token contract address");
        require(balances[from] >= amount, "Low balance");
        balances[from] = balances[from].sub(amount);
        require(allowed[from][msg.sender] >= amount, "Not approved");
        allowed[from][msg.sender] = allowed[from][msg.sender].sub(amount);
        balances[to] = balances[to].add(amount);
        emit Transfer(from, to, amount);
        return true;
    }

    //standard ERC-20 function
    function approve(address spender, uint256 amount)public returns (bool success) { 
        allowed[msg.sender][spender] = amount;
        emit Approval(msg.sender, spender, amount);
        return true;
    }

    //standard ERC-20 function
    function allowance(address owner, address spender) public view returns (uint256 remaining) {
        return allowed[owner][spender];
    }

    function burn(uint256 value) external {
        _totalSupply = _totalSupply.sub(value);
        balances[msg.sender] = balances[msg.sender].sub(value);
        emit Transfer(msg.sender, address(0), value);
    }
}