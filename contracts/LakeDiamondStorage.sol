pragma solidity 0.5.7;


import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "./BaseStorage.sol";
import "./IBurnableERC20.sol";
import "./LakeDiamond.sol";

/**
@title storage contract for LakeDiamond contract
@author Bohdan Grytsenko
@notice contract store all data from main contract to save it in case of redeploy main contract with new code
*/
contract LakeDiamondStorage is BaseStorage, Ownable {

    /// public fields ///

    address public lakeDiamondAddress;
    address public proposalStorageAdapterAddress;
    address public orderStorageAdapterAddress;
    IBurnableERC20 public tokenContract;


    /// events //

    event ProposalStorageAdapterAddressSet(address subOwner);

    event OrderStorageAdapterAddressSet(address subOwner);

    event LakeDiamondAddressSet(address subOwner);

    /// constructors ///

    constructor (IBurnableERC20 token) public {
        require(address(token) != address(0), "token argument can`t be empty");
        tokenContract = token;
    }

    /// modifiers ///
    
    modifier accessRestriction() {
        require(
            msg.sender == proposalStorageAdapterAddress || 
            msg.sender == orderStorageAdapterAddress || 
            msg.sender == lakeDiamondAddress,
            "Only allowed contract can write data to storage"
        );
        _;
    }

    /// public methods ///

    /**
    @notice set new address of main contract, proposal storage adapter and order storage adapter(accessible only from contract owner)
    @param lakeDiamond address of new LakeDiamond contract
    @param proposalStorageAdapter address of new proposal storage adapter contract
    @param orderStorageAdapter address of new order storage adapter contract
    */
    function setLakeDiamondAndAdaptersAddresses(address lakeDiamond, address proposalStorageAdapter, address orderStorageAdapter)
    external onlyOwner() {
        setLakeDiamondAddressInternal(lakeDiamond);
        
        setProposalStorageAdapterAddressInternal(proposalStorageAdapter);
        
        setOrderStorageAdapterAddressInternal(orderStorageAdapter);
    }

    /**
    @notice set new address of main contract(accessible only from contract owner)
    @param contractAddress address of new LakeDiamond contract
    */
    function setLakeDiamondAddress(address contractAddress) external onlyOwner() {
        setLakeDiamondAddressInternal(contractAddress);
    }

    /**
    @notice set new address of proposal storage adapter(accessible only from contract owner)
    @param contractAddress address of new proposal storage adapter contract
    */
    function setProposalStorageAdapterAddress(address contractAddress) external onlyOwner() {
        setProposalStorageAdapterAddressInternal(contractAddress);
    }


    /**
    @notice set new address of order storage adapter(accessible only from contract owner)
    @param contractAddress address of new order storage adapter contract
    */
    function setOrderStorageAdapterAddress(address contractAddress) external onlyOwner() {
        setOrderStorageAdapterAddressInternal(contractAddress);
    }

    /// internal methods ///

    function setLakeDiamondAddressInternal(address contractAddress) internal {
        require(contractAddress != address(0), "New LakeDiamondAddress address must contain non zero address");

        if(lakeDiamondAddress != address(0)) {
            LakeDiamond(lakeDiamondAddress).transferTokensToNewLakeDiamondAddress(contractAddress);
        }

        lakeDiamondAddress = contractAddress;
        emit LakeDiamondAddressSet(contractAddress);
    }

    function setProposalStorageAdapterAddressInternal(address contractAddress) internal {
        require(contractAddress != address(0), "New ProposalStorageAdapterAddress address must contain non zero address");
        proposalStorageAdapterAddress = contractAddress;
        emit ProposalStorageAdapterAddressSet(contractAddress);
    }

    function setOrderStorageAdapterAddressInternal(address contractAddress) internal {
        require(contractAddress != address(0), "New OrderStorageAdapterAddress address must contain non zero address");
        orderStorageAdapterAddress = contractAddress;
        emit OrderStorageAdapterAddressSet(contractAddress);
    }

}