pragma solidity 0.5.7;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "./IBurnableERC20.sol";
import "./LakeDiamondStorage.sol";
import "./ProposalStorageAdapter.sol";
import "./OrderStorageAdapter.sol";

/**
 * @title Main contract with Lakediamond product business logic
 * @author Bohdan Hrytsenko
 * @notice Main contract with Lakediamond product business logic
 */
contract LakeDiamond is Ownable {
    using SafeMath for uint;

    /// public fields ///

    address public backendAddress;
    LakeDiamondStorage public lakeDiamondStorage;
    uint public minProposalTokenAmount;

    bytes32 public constant subOwnerColumnKey = 0xfc0e95ebfbf8d2015707df46adac158069e12b1428ad85b104b59e9ad71f7d26;//keccak256("LakeDiamondSubOwner");
    bytes32 public constant whiteListKey =      0xc3d232a6c0e2fb343117f17a5ff344a1a84769265318c6d7a8d7d9b2f8bb49e3;//keccak256("Whitelist");

    /// events ///

    event SubOwnerAdded(address indexed subOwner);
    event SubOwnerRemoved(address indexed subOwner);

    event OrderTypeSet(uint indexed id, string description);

    event ProposalCreated(uint indexed id, address indexed from, uint price, uint tokenAmount);
    event ProposalWithdrawn(uint indexed id, address indexed withdrownBy, uint tokenAmount);
    event ProposalCompleted(uint indexed id, uint indexed orderId, uint tokenAmount, uint weiAmount, bool isActive);

    event OrderAdded(
        uint indexed orderId,
        address indexed from,
        uint indexed typeId,
        uint price,
        uint weiAmount,
        uint tokenAmount,
        uint exchangeRate,
        string metadata
    );
    event OrderProcessed(uint indexed id, uint rest);

    event WhiteListAddressAdded(address indexed account);
    event WhitelistedAmountChanged(address indexed account, uint allowedTokenAmount);
    event WhiteListAddressRemoved(address indexed account);

    /// constructors ///
    
    constructor (address storageAddress, address backend, uint minTokensAmountInProposal) public {
        require(storageAddress != address(0), "Storage address must contain non zero address");
        lakeDiamondStorage = LakeDiamondStorage(storageAddress);
        setBackendAddress(backend);

        setMinProposalTokenAmount(minTokensAmountInProposal);
    }

    
    /// external metods ///

    /**
    @notice transfer tokens on contract to new LakeDiamond address
    @param newAddress address of new LakeDiamond contract
    */
    function transferTokensToNewLakeDiamondAddress(address newAddress) external onlyLakeDiamondStorage() {
        require(isContract(newAddress), "New address must be contract address");

        IBurnableERC20 tokenContract = getTokenContract();
        require(tokenContract.transfer(newAddress, tokenContract.balanceOf(address(this))), "Can`t transfer tokens");
    }

    /**
    @notice add address to subOwners list(accessible only from contract owner address)
    @param addresses address list to add to subOwners list, address[]
    */
    function addSubOwners(address[] calldata addresses) external onlyOwner() {
        for (uint i = 0; i < addresses.length; i++) {
            bytes32 subOwnerKey = getSubOwnerKey(addresses[i]);
            if (!lakeDiamondStorage.getBool(subOwnerKey)) {
                lakeDiamondStorage.setBool(subOwnerKey, true);
                emit SubOwnerAdded(addresses[i]);
            }
        }
    }

    /**
    @notice remove address from subOwners list(accessible only from contract owner address)
    @param addresses list of addresses to be remove from subOwners list
    */
    function removeSubOwners(address[] calldata addresses) external onlyOwner() {
        for (uint i = 0; i < addresses.length; i++) {
            bytes32 subOwnerKey = getSubOwnerKey(addresses[i]);
            lakeDiamondStorage.deleteBool(subOwnerKey);
            emit SubOwnerRemoved(addresses[i]);
        }
    }

    /**
    @notice create new order record
    @param typeId id of order type, uint
    @param metadata text information about proposal, string
    @param price tokens buyback price, uint
    @param tokenAmount amount of tokens to buyback
    */
    function createOrder(uint typeId, string calldata metadata, uint price, uint tokenAmount, uint exchangeRate) external payable onlySubOwner() {
        require(msg.value > 0, "msg.value must be greater than 0");
        require(exchangeRate > 0, "exchange rate must be greater than 0");
        
        require(price.mul(tokenAmount).mul(1 ether).div(exchangeRate) == msg.value, "msg value doesn't match order parameters");
        
        OrderStorageAdapter orderStorageAdapter = getOrderStorageAdapter();
        require(orderStorageAdapter.getIsOrderTypeExist(typeId), "Order type not exist");

        uint newId = orderStorageAdapter.createOrder(typeId, metadata, price, tokenAmount, msg.value, exchangeRate, msg.sender);
        emit OrderAdded(newId, msg.sender, typeId, price, msg.value, tokenAmount, exchangeRate, metadata);
        
        matchOrder(newId);
    }

    /**
    @notice set address to whitelist with amount of allowed tokens to create proposal(accessible only from backend address)
    @param account address to add to whitelist
    @param allowedTokenAmount amount of tokens allowed to spend, uint
    */
    function addToWhitelist(address account, uint allowedTokenAmount) external onlyBackend() {
        addToWhitelistInternal(account, allowedTokenAmount);
    }

    /**
    @notice set batch addresses to whitelist with amounts of allowed tokens to create proposal(accessible only from backend address)
    @param accounts addresses to add to whitelist
    @param allowedTokenAmounts amounts of tokens allowed to spend, uint
    */
    function batchAddToWhitelist(address[] calldata accounts, uint[] calldata allowedTokenAmounts) external onlyBackend() {
        require(accounts.length == allowedTokenAmounts.length, "accounts array length must be equal to allowed token amounts array length");

        for (uint index = 0; index < accounts.length; index++) {
            addToWhitelistInternal(accounts[index], allowedTokenAmounts[index]);
        }
    }

    /**
    @notice incease amount of allowed tokens to create proposal(accessible only from onlySubOwner address)
    @param account address to add to whitelist
    @param allowedTokenAmount amount of tokens allowed to own, uint
    */
    function increaseAllowance(address account, uint allowedTokenAmount) external onlySubOwner() {
        require(getIsWhitelisted(account), "User address is not in whitelist");
        require(getAllowedTokenAmount(account) < allowedTokenAmount, "allowedTokenAmount must be greater than current allowed token amount");
        
        setAllowedTokenAmount(account, allowedTokenAmount);
    }

    /**
    @notice remove address from whitelist(accessible only from onlySubOwner address)
    @param account address to remove from whitelist
    */
    function removeFromWhitelist(address account) public onlySubOwner() {
        if (getIsWhitelisted(account)) {
            setIsWhitelisted(account, false);
            setAllowedTokenAmount(account, 0);
        }
    }

    /**
    @notice create new proposal
    @param price price per token, uint
    @param tokenAmount amount of tokens in proposal, uint
    @param startPositionId id of proposal from which start to search position to insert
    */
    function createProposal(uint price, uint tokenAmount, uint startPositionId) external {
        require(price != 0, "Price value must be greater than 0");
        require(tokenAmount >= minProposalTokenAmount, "Token amount must be >= min proposal token amount");
        require(getIsWhitelisted(msg.sender), "User address is not in whitelist");
        
        require(getTokenContract().transferFrom(msg.sender, address(this), tokenAmount), "Cant transfer tokens");

        uint allowedAmount = getAllowedTokenAmount(msg.sender);
        require(allowedAmount >= tokenAmount, "You can`t create proposal for more tokens than allowed to your account");
        setAllowedTokenAmount(msg.sender, allowedAmount.sub(tokenAmount));
        
        uint256 id = getProposalStorageAdapter().createProposal(msg.sender, price, tokenAmount, startPositionId);

        emit ProposalCreated(id, msg.sender, price, tokenAmount);
    }

    /**
    @notice withdraw tokens from proposal and close it
    @param id id of proposal to withdraw
    */
    function withdrawProposal(uint id) external {        
        ProposalStorageAdapter proposalStorageAdapter = getProposalStorageAdapter();
        address proposalOwner = proposalStorageAdapter.getProposalOwner(id);
        require(
            proposalOwner == msg.sender || getIsSubOwner(msg.sender), 
            "Withdrawal accessible only from proposal owner address or contract subOwner address"
        );

        withdrawProposalInternal(proposalStorageAdapter, proposalOwner, id, msg.sender);
    }

    /**
    @notice set new order type
    @param id type id, uintsetAllowedTokenAmount
    @param description description of order type, string
    */
    function setOrderType(uint id, string calldata description) external onlySubOwner() {
        getOrderStorageAdapter().setOrderTypeDescription(id, description);
        emit OrderTypeSet(id, description);
    }

    /// public methods ///

    /**
    @notice match proposals to order and send rest of eth back to order`s creator(accessible only from backend address)
    */
    function matchOrder(uint orderId) internal {
        OrderStorageAdapter orderStorageAdapter = getOrderStorageAdapter(); 
 
        uint orderPrice = orderStorageAdapter.getPrice(orderId); 
        uint orderTokenAmount = orderStorageAdapter.getTokenAmount(orderId);
        uint exchangeRate = orderStorageAdapter.getExchangeRate(orderId);
        uint orderWeiAmount = orderStorageAdapter.getWeiAmount(orderId);

        ProposalStorageAdapter proposalStorageAdapter = getProposalStorageAdapter(); 
        IBurnableERC20 tokenContract = getTokenContract();

        uint proposalId = proposalStorageAdapter.getHeadId();
        uint tokensToBurn = 0;

        while (proposalId != 0) {
            uint proposalPrice = proposalStorageAdapter.getPrice(proposalId);
            if (proposalPrice > orderPrice) {
                break;
            }
 
            uint proposalTokenAmount = proposalStorageAdapter.getTokenAmount(proposalId);
            if (proposalTokenAmount > 0) {
                uint withdrawnAmount = proposalTokenAmount >= orderTokenAmount ? orderTokenAmount : proposalTokenAmount;

                uint proposalWeiAmount = withdrawnAmount.mul(proposalPrice).mul(1 ether).div(exchangeRate);

                address payable proposalOwner = proposalStorageAdapter.getProposalOwner(proposalId);
                if (!proposalOwner.send(proposalWeiAmount)) {
                    withdrawProposalInternal(proposalStorageAdapter, proposalOwner, proposalId, msg.sender);

                    proposalId = proposalStorageAdapter.getAfterId(proposalId);
                    continue;
                }

                proposalTokenAmount = proposalTokenAmount.sub(withdrawnAmount);
                proposalStorageAdapter.setTokenAmount(proposalId, proposalTokenAmount);

                orderTokenAmount = orderTokenAmount.sub(withdrawnAmount);
                orderWeiAmount = orderWeiAmount.sub(proposalWeiAmount);
                
                tokensToBurn += withdrawnAmount;
    
                emit ProposalCompleted(proposalId, orderId, withdrawnAmount, proposalWeiAmount, proposalTokenAmount > 0); 
            }

            if(proposalTokenAmount == 0) {
                proposalId = proposalStorageAdapter.getAfterId(proposalId);
            }
            
            if (orderTokenAmount == 0) {
               break;
            }
        }
        
        if(tokensToBurn > 0) {
            tokenContract.burn(tokensToBurn);
        }
        
        proposalStorageAdapter.setHeadId(proposalId);

        orderStorageAdapter.closeOrder(orderId);
        emit OrderProcessed(orderId, orderWeiAmount);
        orderStorageAdapter.getOrderCreatedBy(orderId).transfer(orderWeiAmount);
    }


    /**
    @notice set new backend address(accessible only from contract owner address)
    @param newAddress new backend address
    */
    function setBackendAddress(address newAddress) public onlyOwner() {
        require(newAddress != address(0), "Backend address must contain non zero address");
        backendAddress = newAddress;
    }

    /**
    @notice get amount of allowed tokens to use in proposal
    @param account address to add to whitelist
    @return amount of tokens alowed to use in proposal, uint
    */
    function getAllowedTokenAmount(address account) public view returns(uint) {
        bytes32 whitelistKey = getWhitelistKey(account);
        return lakeDiamondStorage.getUint(whitelistKey);
    }


    /**
    @notice check is address in subOwners list
    @param subOwnerAddress address to check
    @return is address in subOwners list, bool
    */
    function getIsSubOwner(address subOwnerAddress) public view returns(bool) {
        bytes32 subOwnerKey = getSubOwnerKey(subOwnerAddress);
        return lakeDiamondStorage.getBool(subOwnerKey);

    }

    function setMinProposalTokenAmount(uint value) public onlyOwner() {
        require(value >= 100, "value must be >= 100");
        minProposalTokenAmount = value;
    }

    /// internal methods ///

    function getOrderStorageAdapter() internal view returns (OrderStorageAdapter) {
        address orderStorageAdapterAddress = lakeDiamondStorage.orderStorageAdapterAddress();

        require(orderStorageAdapterAddress != address(0), "orderStorageAdapterAddress is not set");
        require(isContract(orderStorageAdapterAddress), "orderStorageAdapterAddress is not a contract");

        return OrderStorageAdapter(orderStorageAdapterAddress);
    }

    function getProposalStorageAdapter() internal view returns (ProposalStorageAdapter) {
        address proposalStorageAdapterAddress = lakeDiamondStorage.proposalStorageAdapterAddress();

        require(proposalStorageAdapterAddress != address(0), "proposalStorageAdapterAddress is not set");
        require(isContract(proposalStorageAdapterAddress), "proposalStorageAdapterAddress is not a contract");

        return ProposalStorageAdapter(proposalStorageAdapterAddress);
    }

    function getTokenContract() internal view returns (IBurnableERC20) {
        return IBurnableERC20(lakeDiamondStorage.tokenContract());
    }
    
    function getWhitelistKey(address account) internal pure returns (bytes32) {
        return keccak256(abi.encode(whiteListKey, account));
    }

    function getSubOwnerKey(address subOwnerAddress) internal pure returns (bytes32) {
        return keccak256(abi.encode(subOwnerColumnKey, subOwnerAddress));
    }

    function addToWhitelistInternal(address account, uint allowedTokenAmount) internal {
        require(!isContract(account), "Address must be not address of contract");
        require(allowedTokenAmount > 0, "allowed token amount must be greater than 0");
        
        if (!getIsWhitelisted(account)) {
            setIsWhitelisted(account, true);
        }

        if (getAllowedTokenAmount(account) < allowedTokenAmount) {
            setAllowedTokenAmount(account, allowedTokenAmount);
        }
    }

    function setAllowedTokenAmount(address account, uint amount) internal {
        bytes32 whitelistKey = getWhitelistKey(account);
        lakeDiamondStorage.setUint(whitelistKey, amount);
        
        emit WhitelistedAmountChanged(account, amount);
    }

    function getIsWhitelisted(address account) internal view returns(bool) {
        bytes32 whitelistKey = getWhitelistKey(account);
        return lakeDiamondStorage.getBool(whitelistKey);
    }

    function setIsWhitelisted(address account, bool value) internal {
        bytes32 whitelistKey = getWhitelistKey(account);
        lakeDiamondStorage.setBool(whitelistKey, value);

        if (value) {
            emit WhiteListAddressAdded(account);
        } else {
            emit WhiteListAddressRemoved(account);
        }
    }

    function isContract(address _addr) private view returns (bool) {
        uint32 size;
        assembly {
            size := extcodesize(_addr)
        }
        return (size > 0);
    }

    function withdrawProposalInternal(
        ProposalStorageAdapter proposalStorageAdapter, address proposalOwner, uint proposalId, address withdrawnBy) internal {        
        
        uint withdrawTokenAmount = proposalStorageAdapter.withdrawProposal(proposalId);
        emit ProposalWithdrawn(proposalId, withdrawnBy, withdrawTokenAmount);

        if(getIsWhitelisted(proposalOwner)) {
            uint allowedTokenAmount = getAllowedTokenAmount(proposalOwner);
            setAllowedTokenAmount(proposalOwner, allowedTokenAmount.add(withdrawTokenAmount));
        }
        
        require(getTokenContract().transfer(proposalOwner, withdrawTokenAmount), "Cant transfer tokens");
    }

    /// modifiers ///
    
    modifier onlyBackend() {
        require(msg.sender == backendAddress, "You can call this method only from backend address");
        _;
    }
    
    modifier onlyLakeDiamondStorage() {
        require(msg.sender == address(lakeDiamondStorage), "You can call this method only from backend address");
        _;
    }
    
    modifier onlySubOwner() {
        require(getIsSubOwner(msg.sender), "You can call this method only from subOwner`s address");
        _;
    }
}