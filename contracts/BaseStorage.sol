pragma solidity 0.5.7;

/**
@title Data storage layer implementation
@author Bohdan Grytsenko
@notice Core contract which implements data storage for every entity
*/
contract BaseStorage {

    /// internal attributes ///

    mapping(bytes32 => uint256)         internal _uIntStorage;
    mapping(bytes32 => string)          internal _stringStorage;
    mapping(bytes32 => address)         internal _addressStorage;
    mapping(bytes32 => address payable) internal _payableAddressStorage;
    mapping(bytes32 => bytes32)         internal _bytes32Storage;
    mapping(bytes32 => bool)            internal _boolStorage;
    mapping(bytes32 => int256)          internal _intStorage;

    /// modifiers ///
    
    modifier accessRestriction() {
        _;
    }

    /// Get Methods ///
    
    /**
    @notice accessor for address attribute type
    @param key storage key value
    */
    function getAddress(bytes32 key) external view returns (address) {
        return _addressStorage[key];
    }
    
    /**
    @notice accessor for payable address attribute type
    @param key storage key value
    */
    function getPayableAddress(bytes32 key) external view returns (address payable) {
        return _payableAddressStorage[key];
    }

    /**
    @notice accessor for uint attribute type
    @param key storage key value
    */
    function getUint(bytes32 key) external view returns (uint) {
        return _uIntStorage[key];
    }

    /**
    @notice accessor for string attribute type
    @param key storage key value
    */
    function getString(bytes32 key) external view returns (string memory) {
        return _stringStorage[key];
    } 

    /**
    @notice accessor for bytes32 attribute type
    @param key storage key value
    */
    function getBytes32(bytes32 key) external view returns (bytes32) {
        return _bytes32Storage[key];
    }

    /**
    @notice accessor for bool attribute type
    @param key storage key value
    */
    function getBool(bytes32 key) external view returns (bool) {
        return _boolStorage[key];
    }

    /**
    @notice accessor for int attribute type
    @param key storage key value
    */
    function getInt(bytes32 key) external view returns (int) {
        return _intStorage[key];
    }    

    /// Set Methods ///

    /**
    @notice modifier for address attribute type
    @param key storage key value
    @param value data to save into storage
    */
    function setAddress(bytes32 key, address value) external accessRestriction() {
        _addressStorage[key] = value;
    }

    /**
    @notice modifier for address зфнфиду attribute type
    @param key storage key value
    @param value data to save into storage
    */
    function setPayableAddress(bytes32 key, address payable value) external accessRestriction() {
        _payableAddressStorage[key] = value;
    }

    /**
    @notice modifier for uint attribute type
    @param key storage key value
    @param value data to save into storage
    */
    function setUint(bytes32 key, uint value) external accessRestriction() {
        _uIntStorage[key] = value;
    }

    /**
    @notice modifier for string attribute type
    @param key storage key value
    @param value data to save into storage
    */
    function setString(bytes32 key, string calldata value) external accessRestriction() {
        _stringStorage[key] = value;
    }

    /**
    @notice modifier for bytes32 attribute type
    @param key storage key value
    @param value data to save into storage
    */
    function setBytes32(bytes32 key, bytes32 value) external accessRestriction() {
        _bytes32Storage[key] = value;
    }
    
    /**
    @notice modifier for bool attribute type
    @param key storage key value
    @param value data to save into storage
    */
    function setBool(bytes32 key, bool value) external accessRestriction() {
        _boolStorage[key] = value;
    }
    
    /**
    @notice modifier for int attribute type
    @param key storage key value
    @param value data to save into storage
    */
    function setInt(bytes32 key, int value) external accessRestriction() {
        _intStorage[key] = value;
    }

    /// Delete Methods ///
    
    /**
    @notice removal of address attribute type
    @param key storage key value
    */
    function deleteAddress(bytes32 key) external accessRestriction() {
        delete _addressStorage[key];
    }
    
    /**
    @notice removal of payable address attribute type
    @param key storage key value
    */
    function deletePayableAddress(bytes32 key) external accessRestriction() {
        delete _addressStorage[key];
    }

    /**
    @notice removal of bytes32 attribute type
    @param key storage key value
    */
    function deleteUint(bytes32 key) external accessRestriction() {
        delete _uIntStorage[key];
    }

    /**
    @notice removal of string attribute type
    @param key storage key value
    */
    function deleteString(bytes32 key) external accessRestriction() {
        delete _stringStorage[key];
    }

    /**
    @notice removal of bytes32 attribute type
    @param key storage key value
    */
    function deleteBytes32(bytes32 key) external accessRestriction() {
        delete _bytes32Storage[key];
    }
    
    /**
    @notice removal of bool attribute type
    @param key storage key value
    */
    function deleteBool(bytes32 key) external accessRestriction() {
        delete _boolStorage[key];
    }
    
    /**
    @notice removal of int attribute type
    @param key storage key value
    */
    function deleteInt(bytes32 key) external accessRestriction() {
        delete _intStorage[key];
    }
}