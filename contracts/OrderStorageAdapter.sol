pragma solidity 0.5.7;


import "./LakeDiamondStorage.sol";


/**
 * @title Order storage and management 
 * @author Bohdan Hrytsenko
 * @notice Single point to manage orders
 */
contract OrderStorageAdapter {
    LakeDiamondStorage public lakeDiamondStorage;

    /// public constants ///

    bytes32 public constant enitityNameKey =        0x21c0107378acb490e7190da71596effe409c128f08adcc5467b293f1f3a66431;//keccak256("order");
    bytes32 public constant orderTypeColumnKey =    0x82d2ef5b3ca74df697ed9417009bba1e755155b5939ec2b359e37dab4aad2de9;//keccak256("orderType");
    bytes32 public constant metadataColumnKey =     0x7a9d3a032b8ff274f09714b56ba8e5ed776ec9638ca303069bc3a3267bb22f65;//keccak256("metadata");
    bytes32 public constant priceColumnKey =        0x282bd803c09c6b34a4d86ee95434129ea89232e91fab09f9e5dc6fe984fa9a6f;//keccak256("price");
    bytes32 public constant weiAmountColumnKey =    0xaaff6a5a99a5df963f892d684fa9b2ac1d6e1eeaead63005c35d454a5c1d20f4;//keccak256("weiAmount");
    bytes32 public constant tokenAmountColumnKey =  0x32df381ac93a61bd1f91ecd61cbf2ca27712cdcb73f6a80b6af9b67ecb84ac78;//keccak256("tokenAmount");
    bytes32 public constant createdByColumnKey =    0x019cc42185861f1f8ffb2d83c35515dd4b74a931013df9a6161bf27ac03e7d64;//keccak256("createdBy");
    bytes32 public constant exchangeRateColumnKey = 0x7888a380828787de51a440b6aa5732844df02a351099e8b1e07332c855a81744;//keccak256("exchangeRate");
    bytes32 public constant isProcessedColumnKey =  0xd9d3a6802c121693a1318e6caba78b6f10a6ad9f88c96eb6de7104af701109b8;//keccak256("isProcessed");
    bytes32 public constant lastOrderIdKey =        0x4414d6e998b6d86846d9b30cf99f607f3e83770bef9a00007529246140f22f4f;//keccak256("lastOrderId");
    bytes32 public constant typeListKey =           0xc6f23743a07ba9b1f3a0299af4e12d23e0d2a7dfcff6c0c8730cfb6fb7d3c094;//keccak256("order.TypeList");

    /// constructors ///

    constructor (LakeDiamondStorage lakeDiamondStorageContract) public {
        require(address(lakeDiamondStorageContract) != address(0), "lakeDiamondStorageContract argument can`t be empty");
        lakeDiamondStorage = lakeDiamondStorageContract;
    }

    /// modifiers ///
    
    modifier lakeDiamondContractOnly() {
        require(
            lakeDiamondStorage.lakeDiamondAddress() == msg.sender,
            "Only LakeDiamond contract can write data to storage"
        );
        _;
    }

    /// external methods ///

    /**
    @notice create new order record
    @param orderType type of order, uint
    @param metadata metadata, string
    @param price maximum price per token, uint
    @param tokenAmount amount of tokens to buy, uint
    @param weiAmount amount of wei to sell, uint
    @param createdBy order owner, address
    @return created recordId
    */
    function createOrder(
        uint orderType,
        string calldata metadata,
        uint price,
        uint tokenAmount,
        uint weiAmount,
        uint exchangeRate,
        address payable createdBy)
        external
        lakeDiamondContractOnly()
        returns(uint orderId) {

        orderId = lakeDiamondStorage.getUint(lastOrderIdKey) + 1;
        bytes32 baseKey = getBaseKey(orderId);

        setOrderType(baseKey, orderType);
        setMetadata(baseKey, metadata);
        setPrice(baseKey, price);
        setTokenAmount(baseKey, tokenAmount);
        setWeiAmount(baseKey, weiAmount);
        setExchangeRate(baseKey, exchangeRate);
        setCreatedBy(baseKey, createdBy);
        
        lakeDiamondStorage.setUint(lastOrderIdKey, orderId);
    }

    /**
    @notice return order record by id
    @param orderId id of order, uint
    @return {
        "orderType": "type of order, uint"
        "metadata": "metadata, string"
        "price": "maximum price per token, uint"
        "weiAmount": "amount of tokens to buy, uint"
        "tokenAmount": "amount of wei to sell, uint"
        "createdBy": "order owner, address"
        "isProcessed": "is order processed, bool"
    }
    */
    function getOrder(uint orderId) 
        external 
        view
        returns(
        uint orderType,
        string memory metadata,
        uint price,
        uint weiAmount,
        uint tokenAmount,
        uint exchangeRate,
        address payable createdBy,
        bool isProcessed) {

        uint lastOrderId = lakeDiamondStorage.getUint(lastOrderIdKey);
        require(lastOrderId >= orderId, "Order not exists");
        
        bytes32 baseKey = getBaseKey(orderId);

        orderType = getOrderType(baseKey);
        metadata = getMetadata(baseKey);
        price = getPrice(baseKey);
        weiAmount = getWeiAmount(baseKey);
        tokenAmount = getTokenAmount(baseKey);
        createdBy = getCreatedBy(baseKey);
        exchangeRate = getExchangeRate(baseKey);
        isProcessed = getIsProcessed(baseKey);
    }

    /**
    @notice close order
    @param orderId id of order
    */
    function closeOrder(uint orderId) external lakeDiamondContractOnly() {
        setIsProcessed(getBaseKey(orderId), true);
    }

    /**
    @notice create new order type
    @param orderTypeId id of type
    @param description ty
    */
    function setOrderTypeDescription(uint orderTypeId, string calldata description) external lakeDiamondContractOnly() {
        lakeDiamondStorage.setString(getOrderTypeKey(orderTypeId), description);
    }

    /**
    @notice check if order type exist
    @param orderTypeId id of type
    */
    function getIsOrderTypeExist(uint orderTypeId) external view returns (bool) {
        string memory description = lakeDiamondStorage.getString(getOrderTypeKey(orderTypeId));
        return bytes(description).length > 0;
    }


    /**
    @notice get order created by field value
    @param orderId id of order
    */
    function getOrderCreatedBy(uint orderId) external view returns (address payable) {
        return getCreatedBy(getBaseKey(orderId));
    }

    /**
    @notice get order price field value
    @param orderId id of order
    */
    function getPrice(uint orderId) external view returns(uint value) {
        bytes32 baseKey = getBaseKey(orderId);
        value = getPrice(baseKey);
    }

    /**
    @notice get order ether amount field value
    @param orderId id of order
    */
    function getWeiAmount(uint orderId) external view returns(uint value) {
        bytes32 baseKey = getBaseKey(orderId);
        value = getWeiAmount(baseKey);
    }

    /**
    @notice get order token amount field value
    @param orderId id of order
    */
    function getTokenAmount(uint orderId) external view returns(uint value) {
        bytes32 baseKey = getBaseKey(orderId);
        value = getTokenAmount(baseKey);
    }

    /**
    @notice get order exchange rate value
    @param orderId id of order
    */
    function getExchangeRate(uint orderId) external view returns(uint value) {
        bytes32 baseKey = getBaseKey(orderId);
        value = getExchangeRate(baseKey);
    }

    /// internal methods ///

    function setOrderType(bytes32 baseKey, uint value) internal {
        bytes32 orderTypeKey = keccak256(abi.encode(baseKey, orderTypeColumnKey));
        lakeDiamondStorage.setUint(orderTypeKey, value);
    }

    function setMetadata(bytes32 baseKey, string memory value) internal {
        bytes32 metadataKey = keccak256(abi.encode(baseKey, metadataColumnKey));
        lakeDiamondStorage.setString(metadataKey, value);
    }

    function setPrice(bytes32 baseKey, uint value) internal {
        bytes32 priceKey = keccak256(abi.encode(baseKey, priceColumnKey));
        lakeDiamondStorage.setUint(priceKey, value);
    }

    function setWeiAmount(bytes32 baseKey, uint value) internal {
        bytes32 weiAmountKey = keccak256(abi.encode(baseKey, weiAmountColumnKey));
        lakeDiamondStorage.setUint(weiAmountKey, value);
    }

    function setTokenAmount(bytes32 baseKey, uint value) internal {
        bytes32 tokenAmountKey = keccak256(abi.encode(baseKey, tokenAmountColumnKey));
        lakeDiamondStorage.setUint(tokenAmountKey, value);
    }

    function setExchangeRate(bytes32 baseKey, uint value) internal {
        bytes32 exchangeRateKey = keccak256(abi.encode(baseKey, exchangeRateColumnKey));
        lakeDiamondStorage.setUint(exchangeRateKey, value);
    }

    function setCreatedBy(bytes32 baseKey, address payable value) internal {
        bytes32 createdByKey = keccak256(abi.encode(baseKey, createdByColumnKey));
        lakeDiamondStorage.setPayableAddress(createdByKey, value);
    }
    
    function setIsProcessed(bytes32 baseKey, bool value) internal {
        bytes32 isProcessedKey = keccak256(abi.encode(baseKey, isProcessedColumnKey));
        lakeDiamondStorage.setBool(isProcessedKey, value);
    }
    
    function getOrderType(bytes32 baseKey) internal view returns(uint value) {
        bytes32 orderTypeKey = keccak256(abi.encode(baseKey, orderTypeColumnKey));
        value = lakeDiamondStorage.getUint(orderTypeKey);
    }

    function getMetadata(bytes32 baseKey) internal view returns(string memory value) {
        bytes32 metadataKey = keccak256(abi.encode(baseKey, metadataColumnKey));
        value = lakeDiamondStorage.getString(metadataKey);
    }

    function getPrice(bytes32 baseKey) internal view returns(uint value) {
        bytes32 priceKey = keccak256(abi.encode(baseKey, priceColumnKey));
        value = lakeDiamondStorage.getUint(priceKey);
    }

    function getWeiAmount(bytes32 baseKey) internal view returns(uint value) {
        bytes32 weiAmountKey = keccak256(abi.encode(baseKey, weiAmountColumnKey));
        value = lakeDiamondStorage.getUint(weiAmountKey);
    }

    function getTokenAmount(bytes32 baseKey) internal view returns(uint value) {
        bytes32 tokenAmountKey = keccak256(abi.encode(baseKey, tokenAmountColumnKey));
        value = lakeDiamondStorage.getUint(tokenAmountKey);
    }

    function getExchangeRate(bytes32 baseKey) internal view returns(uint value) {
        bytes32 exchangeRateKey = keccak256(abi.encode(baseKey, exchangeRateColumnKey));
        value = lakeDiamondStorage.getUint(exchangeRateKey);
    }

    function getCreatedBy(bytes32 baseKey) internal view returns(address payable value) {
        bytes32 createdByKey = keccak256(abi.encode(baseKey, createdByColumnKey));
        value = lakeDiamondStorage.getPayableAddress(createdByKey);
    }

    function getIsProcessed(bytes32 baseKey) internal view returns(bool value) {
        bytes32 isProcessedKey = keccak256(abi.encode(baseKey, isProcessedColumnKey));
        value = lakeDiamondStorage.getBool(isProcessedKey);
    }

    function getBaseKey(uint index) internal pure returns(bytes32) {
        return keccak256(abi.encode(enitityNameKey, index));
    }

    function getOrderTypeKey(uint orderTypeId) internal pure returns (bytes32 orderTypeKey) {
        orderTypeKey = keccak256(abi.encode(typeListKey, orderTypeId));
    }
}