let fs = require("fs");

const fileName = "deployedConfig.json";
let deployedConfig = {};
let isConfigLoaded = false;


const cleanupDeployedConfig = function(network) {
    if (!isConfigLoaded) {
        loadDeployedConfigIntoCache();
    }
    if (network.length == 0){
        throw "network is empty";
    }
    deployedConfig[network] = {};
}

const getNetworkDeployedConfig = (network) => {
    if (!isConfigLoaded) {
        loadDeployedConfigIntoCache();
    }
    if (network.length == 0){
        throw "network is empty";
    }

    if (!deployedConfig[network]) {
        deployedConfig[network] = {};
    }
    return deployedConfig[network];
};

const loadDeployedConfigIntoCache = () => {
    if (fs.existsSync(fileName)) {
        deployedConfig = JSON.parse(fs.readFileSync(fileName)) || {};
    }
    isConfigLoaded = true;
};

const saveDeployedConfig = () => {
    console.log("Saving deployed config");
    if(!isConfigLoaded) {
        throw "Config not loaded! Use loadDeployedConfigIntoCache mod first."
    }
    fs.writeFileSync(fileName, JSON.stringify(deployedConfig, null, 4));
};


const deployedConfigPathConsts = {
    deployedContracts: {
        lakeDiamondStorage: {
            address: {
                path: "deployedContracts.lakeDiamondStorage.address"
            },
            deployer: {
                path: "deployedContracts.lakeDiamondStorage.deployer"
            },
            lakeDiamondAddress: {
                path: "deployedContracts.lakeDiamondStorage.lakeDiamondAddress"
            },
            proposalStorageAdapterAddress: {
                path: "deployedContracts.lakeDiamondStorage.proposalStorageAdapterAddress"
            },
            orderStorageAdapterAddress: {
                path: "deployedContracts.lakeDiamondStorage.orderStorageAdapterAddress"
            }
        },
        lakeDiamond: {
            address: {
                path: "deployedContracts.lakeDiamond.address"
            },
            deployer: {
                path: "deployedContracts.lakeDiamond.deployer"
            },
            subOwners: {
                path: "deployedContracts.lakeDiamond.subOwners"
            },
            backandAddress: {
                path: "deployedContracts.lakeDiamond.backandAddress"
            },
            minProposalTokenAmount: {
                path: "deployedContracts.lakeDiamond.minProposalTokenAmount"
            }
        },
        orderStorageAdapter: {
            address: {
                path: "deployedContracts.orderStorageAdapter.address"
            },
            deployer: {
                path: "deployedContracts.orderStorageAdapter.deployer"
            }
        },
        proposalStorageAdapter: {
            address: {
                path: "deployedContracts.proposalStorageAdapter.address"
            },
            deployer: {
                path: "deployedContracts.proposalStorageAdapter.deployer"
            }
        },
        token: {
            address: {
                path: "deployedContracts.token.address"
            },
            deployer: {
                path: "deployedContracts.token.deployer"
            }
        },
    },
    mainDeployerAddress: {
        path: "deployerAddress"
    }
};

module.exports = { saveDeployedConfig, loadDeployedConfigIntoCache, getNetworkDeployedConfig, cleanupDeployedConfig, deployedConfigPathConsts };