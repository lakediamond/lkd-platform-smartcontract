var HDWalletProvider = require("truffle-hdwallet-provider");
var mnemonic = "much angle uniform proof cram round satisfy wide buyer awesome valley resemble";


module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*" // Match any network id
    },
    rinkebyLocal: {
      provider: function() {
        return new HDWalletProvider(mnemonic, "https://rinkeby.laked.site")
      },
      network_id: 4
    },
    ganache: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*"
    },
    coverage: {
      host: "127.0.0.1",
      network_id: "*",
      port: 8555,         // <-- If you change this, also set the port option in .solcover.js.
      //gas: 0xfffffffffff, // <-- Use this high gas value
      //gasPrice: 0x00      // <-- Use this low gas price
    },
    rinkeby: {
      provider: function() {
        return new HDWalletProvider(mnemonic, "https://rinkeby.infura.io/v3/a2146968c4464936a98c39d9ab88b60d")
      },
      network_id: 4
    }
  },
  compilers: {
     solc: {
       version: "0.5.7",  // ex:  "0.4.20". (Default: Truffle's installed solc),
       settings: {
        optimizer: {
          enabled: false,
          runs: 100000   // Optimize for how many times you intend to run the code
        }
      }
    }
  },
  mocha: {
    enableTimeouts: false
  }
};