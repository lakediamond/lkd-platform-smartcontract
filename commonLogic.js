
const getValueByPath = (object, path, defaultValue = {}) => {
    let value = object;
    const pathParts = path.split(".");
    for (let index = 0; index < pathParts.length -1; index++) {
        const pathPart = pathParts[index];
        
        if (!value[pathPart]) {
            value[pathPart] = {};
        }
        value = value[pathPart];
    }

    const lastPathPart = pathParts.pop();
    if (!value[lastPathPart]) {
        value[lastPathPart] = defaultValue;
    }
    return value[lastPathPart];
};

const setValueByPath = (object, path, data) => {
    let selectedValue = object;
    const pathParts = path.split(".");
    for (let index = 0; index < pathParts.length - 1; index++) {
        const pathPart = pathParts[index];
        
        if (!selectedValue[pathPart]) {
            selectedValue[pathPart] = {};
        }
        selectedValue = selectedValue[pathPart];
    }
    const lastPathPart = pathParts.pop();
    selectedValue[lastPathPart] = data;
};

const combinePath = (pathTemplate, config) => {
    Object.keys(config).forEach(key => {
        const regExp = new RegExp(`{${key}}`,"g");
        pathTemplate = pathTemplate.replace(regExp, config[key]);
    });
    return pathTemplate;
};


const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

const separationString = "=".repeat(10);
const getFormatedConsoleLabel = function(unformatedLable){
    return "\n" + separationString + " " + unformatedLable + " " + separationString + "\n";
};

module.exports = { getValueByPath, setValueByPath, combinePath, uuidv4, getFormatedConsoleLabel };