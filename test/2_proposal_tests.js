const { catchRevert, catchRevertMessage } = require("./exceptions.js");
const utils = require("../utils.js");

const LakeDiamond = artifacts.require("./LakeDiamond.sol");
const TestToken = artifacts.require("./TestToken.sol");
const ProposalStorageAdapter = artifacts.require("./ProposalStorageAdapter.sol");


const { deployedConfigPathConsts, getNetworkDeployedConfig } = require("../deployedConfigHelper");
const { getValueByPath } = require("../commonLogic");

contract('test proposals', function () {
	before('initialize accounts and contracts', async () => {
		const networkId = await utils.getNetworkId(web3);
		
		accounts = await web3.eth.getAccounts();
		LakeDiamondInstance = await LakeDiamond.deployed();
		ProposalStorageAdapterInstance = await ProposalStorageAdapter.deployed();
		TestTokenInstance = await TestToken.deployed();

		let deployedConfig = getNetworkDeployedConfig(networkId);
		
		tokenOwner = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.token.deployer.path);

		owner = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.deployer.path);
			
		subOwners = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.subOwners.path);
			
		backandAddress = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.backandAddress.path);
			
		minProposalTokenAmount = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.minProposalTokenAmount.path);
			
		console.log(
			`owner: "${owner}";\nsubOwners:"${subOwners}";\nbackandAddress: ` + 
			`"${backandAddress}";\ntokenOwner: "${tokenOwner}";\nminProposalTokenAmount: ${minProposalTokenAmount}`
		);
	})

	describe('whitelist', function () {
		it('cant call addAddressToWhitelist from anyone except backend address', async () => {
			await catchRevertMessage(
				LakeDiamondInstance.addToWhitelist(
					owner,
					1000,
					{ from: utils.getNotExistAddress(accounts, [backandAddress]) }
				),
				"You can call this method only from backend address"
			);
		})

		it('call addAddressToWhitelist', async () => {
			await LakeDiamondInstance.addToWhitelist(owner, 1000, { from: backandAddress });

			const allowedTokensAmount = await LakeDiamondInstance.getAllowedTokenAmount(owner);
			assert.equal(allowedTokensAmount, 1000);
		})

		it('call getAllowedTokenAmount', async () => {
			await LakeDiamondInstance.addToWhitelist(owner, 1000, { from: backandAddress });
			const result = await LakeDiamondInstance.getAllowedTokenAmount.call(owner);
			assert.equal(result, 1000);
		})

		it('cant call removeFromWhitelist from anyone except subOwner address', async () => {
			await catchRevertMessage(
				LakeDiamondInstance.removeFromWhitelist(
					owner,
					{ from: utils.getNotExistAddress(accounts, subOwners) }
				),
				"You can call this method only from subOwner`s address"
			);
		})

		it('call removeFromWhitelist', async () => {
			await LakeDiamondInstance.addToWhitelist(owner, 1000, { from: backandAddress });
			
			await LakeDiamondInstance.removeFromWhitelist(owner, { from: subOwners[0] });
		})

		it('can`t call removeFromWhitelist for not whitelisted user', async () => {
			await LakeDiamondInstance.addToWhitelist(owner, 1000, { from: backandAddress });
			await LakeDiamondInstance.removeFromWhitelist(owner, { from: subOwners[0] });

			await catchRevertMessage(
				LakeDiamondInstance.increaseAllowance(owner, 1000, { from: subOwners[0] }),
				"User address is not in whitelist"
			);
		})

		it('cant call increaseAllowance from anyone except subOwner address', async () => {
			await catchRevertMessage(
				LakeDiamondInstance.increaseAllowance(
					owner,
					1000,
					{ from: utils.getNotExistAddress(accounts, subOwners) }
				),
				"You can call this method only from subOwner`s address"
			);
		})

		it('can`t call increaseAllowance for not whitelisted user', async () => {
			await LakeDiamondInstance.addToWhitelist(owner, 1000, { from: backandAddress });
			await LakeDiamondInstance.removeFromWhitelist(owner, { from: subOwners[0] });

			await catchRevertMessage(
				LakeDiamondInstance.increaseAllowance(owner, 1000, { from: subOwners[0] }),
				"User address is not in whitelist"
			);
		})

		it('can`t call increaseAllowance with <= allowedTokenAmount', async () => {
			await LakeDiamondInstance.addToWhitelist(owner, 1000, { from: backandAddress });

			await catchRevertMessage(
				LakeDiamondInstance.increaseAllowance(owner, 1000, { from: subOwners[0] }),
				"allowedTokenAmount must be greater than current allowed token amount"
			);
		})

		it('call increaseAllowance', async () => {
			await LakeDiamondInstance.addToWhitelist(owner, 1000, { from: backandAddress });
			
			await LakeDiamondInstance.increaseAllowance(owner, 1001, { from: subOwners[0] });

			const allowedTokensAmount = await LakeDiamondInstance.getAllowedTokenAmount(owner);
			assert.equal(allowedTokensAmount, 1001);
		})

		
		it('cant call batchAddToWhitelist from anyone except backend address', async () => {
			await catchRevertMessage(
				LakeDiamondInstance.batchAddToWhitelist(
					[owner],
					[1000],
					{ from: utils.getNotExistAddress(accounts, [backandAddress]) }
				),
				"You can call this method only from backend address"
			);
		})

		it('call batchAddToWhitelist', async () => {
			await LakeDiamondInstance.batchAddToWhitelist(accounts, accounts.map(() => 1000), { from: backandAddress });
		})
	})

	describe('create proposal', function () {
		it('cant call createProposal with 0 price', async () => {
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });

			await catchRevertMessage(LakeDiamondInstance.createProposal(0, minProposalTokenAmount, 0, { from: tokenOwner }), "Price value must be greater than 0");
        })
        
		it('cant call createProposal with amount less than minProposalTokenAmount', async () => {
			if (minProposalTokenAmount > 0) {
				await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
				await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });
	
				await catchRevertMessage(
					LakeDiamondInstance.createProposal(10, minProposalTokenAmount - 1, 0, { from: tokenOwner }),
					"Token amount must be >= min proposal token amount"
				);
			}
		})
        
		it('cant call createProposal without tokens', async () => {
			const account = utils.getNotExistAddress(accounts, [tokenOwner]);

			let balance = await TestTokenInstance.balanceOf(account);
			if (balance > 0) {
				console.log(`send all token balance(${balance} LKD) from account(${account}) to tokenOwner(${tokenOwner})`);
				await TestTokenInstance.transfer(tokenOwner, balance, { from: account });
			}
			
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: account });
			await LakeDiamondInstance.addToWhitelist(account, 1000, { from: backandAddress });

			await catchRevertMessage(LakeDiamondInstance.createProposal(10, minProposalTokenAmount, 0, { from: account }), "Low balance");
		})
        
		it('cant call createProposal without approval', async () => {
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });
			await TestTokenInstance.approve(LakeDiamondInstance.address, 0, { from: tokenOwner });

			await catchRevertMessage(LakeDiamondInstance.createProposal(10, minProposalTokenAmount, 0, { from: tokenOwner }), "Not approved");
		})

		it('cant call createProposal of ProposalStorageAdapterInstance instance not from LakeDiamondAddress', async () => {
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });

			await catchRevertMessage(ProposalStorageAdapterInstance.createProposal(tokenOwner, 999, 1000, "0x0", { from: tokenOwner }),
				"Only LakeDiamond contract can write data to storage");
		})
        
		it('cant call createProposal from account not in white list', async () => {
			await TestTokenInstance.approve(
				LakeDiamondInstance.address,
				1000,
				{ from: tokenOwner }
			);
			await LakeDiamondInstance.removeFromWhitelist(tokenOwner, { from: subOwners[0] });

			await catchRevertMessage(LakeDiamondInstance.createProposal(10, minProposalTokenAmount, 0, { from: tokenOwner }),
				"User address is not in whitelist");
		})
        
		it('call createProposal', async () => {
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });

			await LakeDiamondInstance.createProposal(10, minProposalTokenAmount, 0, { from: tokenOwner });
		})
        
		it('cant create order with hint to inactive proposal', async () => {
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });

			const id = await utils.createProposal(LakeDiamondInstance, 1, 100, { from: tokenOwner });
			LakeDiamondInstance.withdrawProposal(id, { from: tokenOwner });

			await catchRevertMessage(
				LakeDiamondInstance.createProposal(100, minProposalTokenAmount, id, { from: tokenOwner }),
				"Proposal inactive or not exist"
			);
		})
        
		it('check allowed tokens after createProposal call', async () => {
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });

			await LakeDiamondInstance.createProposal(10, minProposalTokenAmount, 0, { from: tokenOwner });

			let allowedTokensAmount = await LakeDiamondInstance.getAllowedTokenAmount(tokenOwner);
			assert.equal(allowedTokensAmount, 1000 - minProposalTokenAmount);
		})
	})
	
	describe('read proposal data', function () {
		it('cant call getProposal with incorrect index', async () => {
			await catchRevertMessage(ProposalStorageAdapterInstance.getProposal.call(9999), "Proposal not exists");
		})

		it('call getProposal', async () => {
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });

			const proposalId = await utils.createProposal(LakeDiamondInstance, 1, minProposalTokenAmount, { from: tokenOwner });
			const result = await ProposalStorageAdapterInstance.getProposal.call(proposalId);

			assert.equal(result[0], tokenOwner);			//owner
			assert.equal(result[1], 100);					//price
			assert.equal(result[2], minProposalTokenAmount);//amount
			assert.equal(result[3].toString(), 0);			//afterId
		})
	})

	describe('withdraw proposal', function () {
		it('cant call withdrawProposal not from LikeDiamond subOwner or proposal owner', async () => {
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });

			const proposalId = await utils.createProposal(LakeDiamondInstance, 0.1, 100, { from: tokenOwner });

			await catchRevertMessage(
				LakeDiamondInstance.withdrawProposal(
					proposalId,
					{
						from: utils.getNotExistAddress(accounts, [tokenOwner, ...subOwners])
					}
				),
				"Withdrawal accessible only from proposal owner address or contract subOwner address"
			);
        })
        
		it('call withdrawProposal', async () => {
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });
			const proposalId = await utils.createProposal(LakeDiamondInstance, 0.1, minProposalTokenAmount, { from: tokenOwner });

			await LakeDiamondInstance.withdrawProposal(proposalId, { from: tokenOwner });
        })
        
		it('call withdrawProposal from subOwner', async () => {
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });
			const proposalId = await utils.createProposal(LakeDiamondInstance, 0.1, minProposalTokenAmount, { from: tokenOwner });

			await LakeDiamondInstance.withdrawProposal(proposalId, { from: subOwners[0] });
        })
        
		it('check allowed tokens after withdraw', async () => {
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });

			const proposalId = await utils.createProposal(LakeDiamondInstance, 0.1, minProposalTokenAmount, { from: tokenOwner });

			let allowedTokensAmount = await LakeDiamondInstance.getAllowedTokenAmount(tokenOwner);
			assert.equal(allowedTokensAmount, 1000 - minProposalTokenAmount);

			await LakeDiamondInstance.withdrawProposal(proposalId, { from: tokenOwner });

			allowedTokensAmount = await LakeDiamondInstance.getAllowedTokenAmount(tokenOwner);
			assert.equal(allowedTokensAmount, 1000);
        })
        
		it('validate sorted list after withdraw', async () => {
			await utils.withdrawAllProposals();

			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: tokenOwner });
			await LakeDiamondInstance.addToWhitelist(tokenOwner, 1000, { from: backandAddress });
			
			const proposal1Id = await utils.createProposal(LakeDiamondInstance, 0.1, minProposalTokenAmount, { from: tokenOwner });

			const proposal2Id = await utils.createProposal(LakeDiamondInstance, 0.2, minProposalTokenAmount, { from: tokenOwner });
			
			const proposal3Id = await utils.createProposal(LakeDiamondInstance, 0.15, minProposalTokenAmount, { from: tokenOwner });

			const proposal4Id = await utils.createProposal(LakeDiamondInstance, 0.3, minProposalTokenAmount, { from: tokenOwner });

			assert.equal(
				(await ProposalStorageAdapterInstance.getAfterId(proposal1Id)).toString(),
				proposal3Id.toString()
			);

			assert.equal(
				(await ProposalStorageAdapterInstance.getAfterId(proposal3Id)).toString(),
				proposal2Id.toString()
			);

			assert.equal(
				(await ProposalStorageAdapterInstance.getAfterId(proposal2Id)).toString(),
				proposal4Id.toString()
			);

			await LakeDiamondInstance.withdrawProposal(proposal2Id, { from: tokenOwner });
			
			assert.equal(
				(await ProposalStorageAdapterInstance.getAfterId(proposal1Id)).toString(),
				proposal3Id.toString()
			);
			
			assert.equal(
				(await ProposalStorageAdapterInstance.getAfterId(proposal3Id)).toString(),
				proposal4Id.toString()
			);
		})
		
		it('validate headId after withdraw first proposal', async () => {
			await utils.withdrawAllProposals();

			let headId = await ProposalStorageAdapterInstance.getHeadId();
			assert.equal(headId, 0);

			const firstProposalid = await utils.createProposal(LakeDiamondInstance, 0.1, minProposalTokenAmount, { from: tokenOwner });
			const secondProposalid = await utils.createProposal(LakeDiamondInstance, 0.1, minProposalTokenAmount, { from: tokenOwner });

			headId = await ProposalStorageAdapterInstance.getHeadId();
			assert.equal(headId.toString(), firstProposalid.toString());

			LakeDiamondInstance.withdrawProposal(firstProposalid);

			headId = await ProposalStorageAdapterInstance.getHeadId();
			assert.equal(headId.toString(), secondProposalid.toString());
		})
		
		it('validate headId after withdraw last proposal', async () => {
			await utils.withdrawAllProposals();

			let headId = await ProposalStorageAdapterInstance.getHeadId();
			assert.equal(headId, 0);

			const id = await utils.createProposal(LakeDiamondInstance, 0.1, minProposalTokenAmount, { from: tokenOwner });

			headId = await ProposalStorageAdapterInstance.getHeadId();
			assert.equal(headId.toString(), id.toString());

			LakeDiamondInstance.withdrawProposal(id);

			headId = await ProposalStorageAdapterInstance.getHeadId();
			assert.equal(headId, 0);
		})
    })
})