const utils = require("../utils.js");

const LakeDiamond = artifacts.require("./LakeDiamond.sol");
const OrderStorageAdapter = artifacts.require("./OrderStorageAdapter.sol");
const ProposalStorageAdapter = artifacts.require("./ProposalStorageAdapter.sol");
const TestToken = artifacts.require("./TestToken.sol");

const { deployedConfigPathConsts, getNetworkDeployedConfig } = require("../deployedConfigHelper");
const { getValueByPath } = require("../commonLogic");

contract('Gas consumption', function () {
	before('initialize accounts and contracts', async () => {
		const networkId = await utils.getNetworkId(web3);
		
		accounts = await web3.eth.getAccounts();
		try {
			LakeDiamondInstance = await LakeDiamond.deployed();
			ProposalStorageAdapterInstance = await ProposalStorageAdapter.deployed();
			OrderStorageAdapterInstance = await OrderStorageAdapter.deployed();
			TestTokenInstance = await TestToken.deployed();
		} catch (e) {
			console.log(e);
			assert.ok(false);
		}

		let deployedConfig = getNetworkDeployedConfig(networkId);

		owner = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.deployer.path);

		tokenOwner = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.token.deployer.path);
			
		subOwners = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.subOwners.path);
			
		backandAddress = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.backandAddress.path);
			
		console.log(
			`owner: "${owner}";\n subOwners: "${subOwners}";\n backandAddress: ` + 
			`"${backandAddress}";\n tokenOwner: "${tokenOwner}";\n`
		);
	})

	describe(`Gas consumption`, () => {
		
		it('call matchOrder with bunch of proposals', async () => {
            const proposalsCount = 110;
			
			const proposalsOwner = tokenOwner;
			await TestTokenInstance.transfer(proposalsOwner, 10000000, { from: tokenOwner });
			await TestTokenInstance.approve(LakeDiamondInstance.address, 10000000, { from: proposalsOwner });
			await LakeDiamondInstance.addToWhitelist(proposalsOwner, 10000000, { from: backandAddress });

			let maxGasConsumption = 0;
            let gasConsumptionAmount = 0;
            
            let createdProposals = [];

            const proposalAmount = 100;

            let headId = 0;
            console.log(`creating ${proposalsCount} proposals`);
			for (let i = 0; i < proposalsCount; i++) {
                createdProposals = createdProposals.sort((a, b) => { return a.price - b.price });

                headId = await ProposalStorageAdapterInstance.getHeadId();

                const price = 1 + Math.round(Math.random() * proposalsCount);
                
                let startIndex = createdProposals.filter((proposal) => { return proposal.price <= price }).length - 10;
                const startId = startIndex >= 0 ? createdProposals[startIndex].id : 0;
				
				let result = await LakeDiamondInstance.createProposal(price, proposalAmount, startId, { from: proposalsOwner });
                
                const createdProposalId = result.logs[1].args["id"];
                console.log(`new proposal id: ${createdProposalId}`);

                createdProposal = {
                    id: createdProposalId,
                    price: price
                }

                createdProposals.push(createdProposal);

                maxGasConsumption = Math.max(maxGasConsumption, result.receipt.cumulativeGasUsed);

                if (result.receipt.cumulativeGasUsed > 500000) {
                    console.log(`create proposal get a lot of gas. Gas consumption: ${result.receipt.cumulativeGasUsed}, ` +
                        `proposal: ${JSON.stringify(createdProposal)}, startId: ${startId}`);
                }

				gasConsumptionAmount += result.receipt.cumulativeGasUsed;
			}
			console.log(`max gas consumption: ${maxGasConsumption}, avg gas comsumption: ${gasConsumptionAmount / proposalsCount}`);
            console.log("Proposals for gas consumption test created");
            

            var props = [];

            let i = 0;
            var proposalId = headId.toString();
            while (proposalId.toString() != "0" && i < 200) {
                let proposal = await ProposalStorageAdapterInstance.getProposal(proposalId);

                proposal.id = proposalId;
                props.push(proposal);

                console.log(`index: ${i++}, id: ${proposalId.toString()}, price: ${proposal.price.toString()},` + 
                    `token amount: ${proposal.tokenAmount}, beforeId: ${proposal.beforeId}, afterId: ${proposal.afterId}`);

                proposalId = proposal.afterId.toString();
            }

            for (let i = 0; i < props.length; i++) {
                var correct = true;

                if (i > 0) {
                    correct = correct && props[i-1].id == props[i].beforeId;
                    correct = correct && props[i-1].price.lte(props[i].price);
                    if (props[i-1].price.eq(props[i].price)) {
                        correct = correct && Number(props[i-1].id) < Number(props[i].id);
                    }
                }
                
                if (i < props.length - 1) {
                    correct = correct && props[i+1].id == props[i].afterId;
                    correct = correct && props[i+1].price.gte(props[i].price);
                }
                if (!correct) {
                    throw new Error("incorrect sorted list");
                }
            }

            const orderPrice = 1 + proposalsCount;

            try {
                await LakeDiamondInstance.setOrderType(1, "Test type");

                const order1TokensAmount = 100 * proposalAmount;
                const order1ExchangeRate = 10000;
                const order1WeiAmount = orderPrice * order1TokensAmount / order1ExchangeRate * Math.pow(10, 18);
                const matchOrderTransactionResult = await LakeDiamondInstance
                    .createOrder(1, "metadata", orderPrice, order1TokensAmount, order1ExchangeRate, { value: order1WeiAmount });
                
                console.log(`transaction gas consumption with ${order1TokensAmount} tokens amount: ${matchOrderTransactionResult.receipt.cumulativeGasUsed}`);
                
                headId = await ProposalStorageAdapterInstance.getHeadId();
                console.log(`headId: ${headId}`);
    
                const order2TokensAmount = 100 * proposalAmount;
                const order2ExchangeRate = 10000;
                const order2WeiAmount = orderPrice * order2TokensAmount / order2ExchangeRate * Math.pow(10, 18);
                const matchOrderTransactionResult2 = await LakeDiamondInstance
                    .createOrder(1, "metadata", orderPrice, order2TokensAmount, order2ExchangeRate, { value: order2WeiAmount });
                
                console.log(`transaction gas consumption with ${order2TokensAmount} tokens amount: ${matchOrderTransactionResult2.receipt.cumulativeGasUsed}`);
                
            } catch (error) {
                console.log(error);
            }

            headId = await ProposalStorageAdapterInstance.getHeadId();
            console.log(`headId: ${headId}`);
			
        });
    

	})
})
