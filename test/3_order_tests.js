const { catchRevert, catchRevertMessage } = require("./exceptions.js");
const utils = require("../utils.js");
const BN = require('bn.js');

const LakeDiamond = artifacts.require("./LakeDiamond.sol");
const OrderStorageAdapter = artifacts.require("./OrderStorageAdapter.sol");
const ProposalStorageAdapter = artifacts.require("./ProposalStorageAdapter.sol");
const TestToken = artifacts.require("./TestToken.sol");

const { deployedConfigPathConsts, getNetworkDeployedConfig } = require("../deployedConfigHelper");
const { getValueByPath } = require("../commonLogic");

contract('test order creating', function () {
	before('initialize accounts and contracts', async () => {
		const networkId = await utils.getNetworkId(web3);
		
		accounts = await web3.eth.getAccounts();
		try {
			LakeDiamondInstance = await LakeDiamond.deployed();
			ProposalStorageAdapterInstance = await ProposalStorageAdapter.deployed();
			OrderStorageAdapterInstance = await OrderStorageAdapter.deployed();
			TestTokenInstance = await TestToken.deployed();
		} catch (e) {
			console.log(e);
			assert.ok(false);
		}

		let deployedConfig = getNetworkDeployedConfig(networkId);

		owner = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.deployer.path);

		tokenOwner = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.token.deployer.path);
			
		subOwners = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.subOwners.path);
			
		backandAddress = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.backandAddress.path);
			
		console.log(
			`owner: "${owner}";\n subOwners: "${subOwners}";\n backandAddress: ` + 
			`"${backandAddress}";\n tokenOwner: "${tokenOwner}";\n `
		);
	})
	
	describe('create order', function () {
		it('cant call createOrder with not created order type', async () => {
			await catchRevertMessage(				
				utils.createOrder(
					LakeDiamondInstance,
					111,		//type id
					"metadata", //metadata
					1.1,		//price
					10,			//tokenAmount
					10,			//exchangeRate
					{
						from: subOwners[0]
					}
				),
				"Order type not exist"
			);
		})
		
		it('call setOrderType', async () => {
			await LakeDiamondInstance.setOrderType(1, "Test type");
		})
		
        
		it('cant call setOrderType from anyone except sub owner', async () => {
			await LakeDiamondInstance.setOrderType(1, "Test type");
			await catchRevertMessage(
				LakeDiamondInstance.setOrderType(
					1,
					"Test type",
					{ from: utils.getNotExistAddress(accounts, subOwners) }
				),
				"You can call this method only from subOwner`s address"
			);
		})
		
		it('cant call createOrder from anyone except sub owner', async () => {
			await LakeDiamondInstance.setOrderType(1, "Test type");

			await catchRevertMessage(
				utils.createOrder(
					LakeDiamondInstance,
					1,			//type id
					"metadata", //metadata
					50,			//price
					10,			//tokenAmount
					10,			//exchangeRate
					{
						from: utils.getNotExistAddress(accounts, subOwners)
					}
				),
				"You can call this method only from subOwner`s address"
			);
		})

		it('cant call createOrder of OrderStorageAdapter instance not from LakeDiamondAddress', async () => {
			await LakeDiamondInstance.setOrderType(1, "Test type");

			await catchRevertMessage(
				OrderStorageAdapterInstance.createOrder(
					1,			//type id
					"metadata",	//metadata
					5,			//price
					10,			//tokenAmount
					100,		//wei amount
					100, 		//exchangeRate
					owner
				),
				"Only LakeDiamond contract can write data to storage"
			);
		})
        
		it('can`t call createOrder with incorrect parameters', async () => {
			const typeId = 1;
			await LakeDiamondInstance.setOrderType(typeId, "Test type");

			const sendConfig = { value: 500, from: subOwners[0] };
			const price = 100;
			const tknAmount = 50;

            await catchRevertMessage(
				LakeDiamondInstance.createOrder(typeId, "metadata", price, tknAmount, Math.round(price * tknAmount / 1000), sendConfig),
				"msg value doesn't match order parameters"
			);
		})
        
		it('call createOrder', async () => {
			const price = 0.15;			
			const exchangeRate = 320.15;
			const tokenAmount = 200;

			await LakeDiamondInstance.setOrderType(1, "Test type");
			await utils.createOrder(LakeDiamondInstance, 1, "metadata", price, tokenAmount, exchangeRate, { from: subOwners[0] });
		})
	})

	describe('read order data', function () {
		it('cant call getOrder with incorrect index', async () => {
			await catchRevertMessage(OrderStorageAdapterInstance.getOrder.call(9999), "Order not exists");
		})

		it('call getOrder', async () => {
			const orderId = 
				await utils.createOrder(LakeDiamondInstance, 
					1,			//type id
					"metadata",	//metadata
					0.8,		//price
					50,			//tokenAmount
					75,			//exchangeRate
					{ from: subOwners[0] }
				);
			
			const result = await OrderStorageAdapterInstance.getOrder(orderId);

			assert.equal(result[0], 1)				//typeId
			assert.equal(result[1], "metadata")		//metadata
			assert.equal(result[2], 80)				//pricec v bn
			assert.equal(result[4], 50)				//token amount
			assert.equal(result[5], 7500)				//exchange rate
			assert.equal(result[6], subOwners[0])	//owner
			assert.equal(result[7], true)			//isProcessed

		})
	})

	describe('SubOwners', function () {
		it('cant call addSubOwners not from contract owner', async () => {
			await catchRevert(
				LakeDiamondInstance.addSubOwners([ accounts[1] ], { from: utils.getNotExistAddress(accounts, [owner]) }),

			);
		})

		it('call addSubOwners', async () => {
			await LakeDiamondInstance.addSubOwners([accounts[2]], { from: owner });
		})

		
		it('cant call removeSubOwners not from contract owner', async () => {
			await catchRevert(
				LakeDiamondInstance.removeSubOwners([accounts[2]], { from: utils.getNotExistAddress(accounts, [owner]) })
			);
		})

		
		it('call removeSubOwners from contract owner', async () => {
			await LakeDiamondInstance.removeSubOwners([accounts[2]], { from: owner });
		})
	})

	describe('create and match order', function () {
		it('matchOrder without proposals', async () => {
			await utils.withdrawAllProposals();

			utils.createOrder(
				LakeDiamondInstance,
				1,			//type id
				"metadata", //metadata
				50,			//price
				100,			//tokenAmount
				100,			//exchangeRate
				{
					from: subOwners[0]
				}
			);
		})
		
		it('matchOrder with proposals', async () => {
			await LakeDiamondInstance.setOrderType(1, "Test type");
			await utils.withdrawAllProposals();

			const orderTokenPrice = 2;
			const orderTokenAmount = 200;
			const exchangeRate = 10;
			
			const proposalsOwner = utils.getNotExistAddress(accounts, [tokenOwner, backandAddress, subOwners[0]]);
			await TestTokenInstance.transfer(proposalsOwner, 1000, { from: tokenOwner });
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: proposalsOwner });
			await LakeDiamondInstance.addToWhitelist(proposalsOwner, 1000, { from: backandAddress });

			const proposalOwnerTokenBalanceBefore = await TestTokenInstance.balanceOf(proposalsOwner);

			const firstProposalPrice = 0.9;
			const firstProposalAmount = 100;
			await utils.createProposal(
				LakeDiamondInstance, firstProposalPrice, firstProposalAmount, { from: proposalsOwner });

			const secondProposalPrice = 0.8;
			const secondProposalAmount = 120;
			await utils.createProposal(
				LakeDiamondInstance, secondProposalPrice, secondProposalAmount, { from: proposalsOwner });
			
			const proposalOwnerTokenBalanceAfter = await TestTokenInstance.balanceOf(proposalsOwner);
			assert(
				proposalOwnerTokenBalanceBefore.eq(
					proposalOwnerTokenBalanceAfter.addn(firstProposalAmount).addn(secondProposalAmount))
			);

			const proposalOwnerBalanceBefore = await web3.eth.getBalance(proposalsOwner);
			const tokenTotalSupplyBefore = await TestTokenInstance.totalSupply();
			
			await utils.createOrder(
				LakeDiamondInstance,
				1,
				"metadata",
				orderTokenPrice,
				orderTokenAmount,
				exchangeRate,
				{ from: subOwners[0] }
			);

			const proposalOwnerBalance = await web3.eth.getBalance(proposalsOwner);

			const etherMultiplier = new BN(10).pow(new BN(18));

			const secondProposalIncome = new BN(secondProposalAmount)
				.mul(new BN(secondProposalPrice * 100))
				.mul(etherMultiplier)
				.div(new BN(exchangeRate * 100));

			const firstProposalIncome = new BN(Math.min(firstProposalAmount, orderTokenAmount - secondProposalAmount))
				.mul(new BN(firstProposalPrice * 100))
				.mul(etherMultiplier)
				.div(new BN(exchangeRate * 100));
			
			const expectedBalanceDiff = firstProposalIncome.add(secondProposalIncome);

			assert(
				new BN(proposalOwnerBalance)
				.sub(new BN(proposalOwnerBalanceBefore))
				.eq(new BN(expectedBalanceDiff))
			);

			const tokenTotalSupplyAfter = await TestTokenInstance.totalSupply();
			assert(tokenTotalSupplyBefore.subn(orderTokenAmount).eq(tokenTotalSupplyAfter));
		})
		
		it('match 1 big proposal in 2 orders', async () => {
			await LakeDiamondInstance.setOrderType(1, "Test type");
			await utils.withdrawAllProposals();

			const orderTokenPrice = 2.1;
			const orderTokenAmount = 200;
			const exchangeRate = 10;
			await utils.createOrder(
				LakeDiamondInstance,
				1,
				"metadata",
				orderTokenPrice,
				orderTokenAmount,
				exchangeRate,
				{ from: subOwners[0] }
			);
			
			const proposalsOwner = utils.getNotExistAddress(accounts, [tokenOwner, backandAddress, subOwners[0]]);
			await TestTokenInstance.transfer(proposalsOwner, 1000, { from: tokenOwner });
			await TestTokenInstance.approve(LakeDiamondInstance.address, 1000, { from: proposalsOwner });
			await LakeDiamondInstance.addToWhitelist(proposalsOwner, 1000, { from: backandAddress });

			const proposalOwnerTokenBalanceBefore = await TestTokenInstance.balanceOf(proposalsOwner);

			const proposalPrice = 0.9;
			const proposalAmount = 2 * orderTokenAmount;
			proposalId = await utils.createProposal(LakeDiamondInstance, proposalPrice, proposalAmount, { from: proposalsOwner });
			
			const proposalOwnerTokenBalanceAfter = await TestTokenInstance.balanceOf(proposalsOwner);
			assert(
				proposalOwnerTokenBalanceBefore.eq(
					proposalOwnerTokenBalanceAfter.addn(proposalAmount))
			);

			const proposalOwnerBalanceBefore = await web3.eth.getBalance(proposalsOwner);
			
			await utils.createOrder(
				LakeDiamondInstance,
				1,
				"metadata",
				orderTokenPrice,
				orderTokenAmount,
				exchangeRate,
				{ from: subOwners[0] }
			);

			await utils.createOrder(
				LakeDiamondInstance,
				1,
				"metadata",
				orderTokenPrice,
				orderTokenAmount,
				exchangeRate,
				{ from: subOwners[0] }
			);

			const proposalOwnerBalance = await web3.eth.getBalance(proposalsOwner);

			const etherMultiplier = new BN(10).pow(new BN(18));

			const proposalIncome = new BN(proposalAmount)
				.mul(new BN(proposalPrice * 100))
				.mul(etherMultiplier)
				.div(new BN(exchangeRate * 100));

			assert(
				new BN(proposalOwnerBalance)
				.sub(new BN(proposalOwnerBalanceBefore))
				.eq(new BN(proposalIncome))
			);
		})
		
	})
})
