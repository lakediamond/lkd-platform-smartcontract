const { catchRevert, catchRevertMessage} = require("./exceptions.js");
const utils = require("../utils.js");

const LakeDiamond = artifacts.require("./LakeDiamond.sol");
const LakeDiamondStorage = artifacts.require("./LakeDiamondStorage.sol");
const OrderStorageAdapter = artifacts.require("./OrderStorageAdapter.sol");
const ProposalStorageAdapter = artifacts.require("./ProposalStorageAdapter.sol");
const TestToken = artifacts.require("./TestToken.sol");


const { deployedConfigPathConsts, getNetworkDeployedConfig } = require("../deployedConfigHelper");
const { getValueByPath } = require("../commonLogic");

contract('test deployment result', function () {

	before('initialize accounts and contracts', async () => {
        const networkId = await utils.getNetworkId(web3);
		accounts = await web3.eth.getAccounts();

		let deployedConfig = getNetworkDeployedConfig(networkId); 
		
		tokenOwner = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.token.deployer.path);

		storageOwner = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamondStorage.deployer.path);

		owner = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.deployer.path);
			
		subOwners = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.subOwners.path);
			
		backandAddress = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.backandAddress.path);
			
		minProposalTokenAmount = getValueByPath(deployedConfig,
			deployedConfigPathConsts.deployedContracts.lakeDiamond.minProposalTokenAmount.path);
			
		console.log(
			`owner: "${owner}";\nsubOwners:"${subOwners}";\nbackandAddress: ` + 
			`"${backandAddress}";\ntokenOwner: "${tokenOwner}";\nminProposalTokenAmount: ` + 
			`${minProposalTokenAmount};\nstorageOwner: ${storageOwner}`
		);
	})

	describe('catch contracts instance', function () {		
		it('catch token contract instance', async () => {
			try {
				TokenInstance = await TestToken.deployed();
			} catch (e) {
				console.log(e);
				assert.ok(false);
			}
		})

		it('catch storage contract instance', async () => {
			try {
				StorageInstance = await LakeDiamondStorage.deployed();
			} catch (e) {
				console.log(e);
				assert.ok(false);
			}
		})

		it('catch LakeDiamond contract instance', async () => {
			try {
				LakeDiamondInstance = await LakeDiamond.deployed();
			} catch (e) {
				console.log(e);
				assert.ok(false);
			}
		})

		it('catch ProposalStorageAdapter contract instance', async () => {
			try {
				ProposalStorageAdapterInstance = await ProposalStorageAdapter.deployed();
			} catch (e) {
				console.log(e);
				assert.ok(false);
			}
		})

		it('catch OrderStorageAdapter contract instance', async () => {
			try {
				OrderStorageAdapterInstance = await OrderStorageAdapter.deployed();
			} catch (e) {
				console.log(e);
				assert.ok(false);
			}
		})
	})

	describe('check constructor setup', function() {
		it('check storage constructor', async () => {
			let erc20Address = await StorageInstance.tokenContract.call();
			assert.ok(erc20Address == TokenInstance.address);
		})

		it('check lakediamond constructor', async () => {
			let storageAddress = await LakeDiamondInstance.lakeDiamondStorage.call();
			let backendAddress = await LakeDiamondInstance.backendAddress.call();

			assert.ok(storageAddress == StorageInstance.address);
			assert.ok(backendAddress == backandAddress);

		})
	})

	describe('check lakeDiamondAddress function in storage contract', function () {
		it ('cant call setLakeDiamondAddress from anyone except owner', async () => {
			await catchRevert(
				StorageInstance.setLakeDiamondAddress(
					LakeDiamondInstance.address,
					{ from: utils.getNotExistAddress(accounts, [storageOwner]) }
				)
			);
		})

		it ('call lakeDiamondAddress from owner but with address(0)', async() => {
			await catchRevertMessage(
				StorageInstance.setLakeDiamondAddress(utils.addressEmpty),
				"New LakeDiamondAddress address must contain non zero address"
			);
		})

		it ('call lakeDiamondAddress from owner', async () => {
			let lakeDiamondAddress = await StorageInstance.lakeDiamondAddress.call();
			assert.ok(lakeDiamondAddress == LakeDiamondInstance.address);
		})
	})


	describe('check proposalStorageAdapterAddress function in storage contract', function () {
		it ('call proposalStorageAdapterAddress from anyone except owner', async () => {
			await catchRevert(
				StorageInstance.setProposalStorageAdapterAddress(
					ProposalStorageAdapterInstance.address,
					{ from: utils.getNotExistAddress(accounts, [storageOwner]) }
				)
			);
		})

		it ('call proposalStorageAdapterAddress from owner but with address(0)', async() => {
			await catchRevertMessage(StorageInstance.setProposalStorageAdapterAddress(utils.addressEmpty), "New ProposalStorageAdapterAddress address must contain non zero address");
		})

		it ('call proposalStorageAdapterAddress from owner', async () => {
			let proposalStorageAdapterAddress = await StorageInstance.proposalStorageAdapterAddress.call();
			assert.ok(proposalStorageAdapterAddress == ProposalStorageAdapterInstance.address);
		})
	})


	describe('check orderStorageAdapterAddress function in storage contract', function () {
		it ('call orderStorageAdapterAddress from anyone except owner', async () => {
			await catchRevert(
				StorageInstance.setOrderStorageAdapterAddress(
					OrderStorageAdapterInstance.address,
					{ from: utils.getNotExistAddress(accounts, [storageOwner]) }
				)
			);
		})

		it ('call orderStorageAdapterAddress from owner but with address(0)', async() => {
			await catchRevertMessage(StorageInstance.setOrderStorageAdapterAddress(utils.addressEmpty),
				"New OrderStorageAdapterAddress address must contain non zero address");
		})

		it ('call orderStorageAdapterAddress from owner', async () => {
			try {
				let orderStorageAdapterAddress = await StorageInstance.orderStorageAdapterAddress.call();
				assert.ok(orderStorageAdapterAddress == OrderStorageAdapterInstance.address);
			} catch (e) {
				console.log(e);
				assert.ok(false);
			}
		})
	})

	describe('Backend address', function () {
		it('cant set new backend address from anyone except owner', async () => {
			await catchRevert(
					LakeDiamondInstance.setBackendAddress(
						owner,
						{ from: utils.getNotExistAddress(accounts, [owner]) }
					),
				);
		})
		
		it('cant set empty backend address', async () => {
			await catchRevertMessage(
				LakeDiamondInstance.setBackendAddress(utils.addressEmpty, { from: owner }),
				"Backend address must contain non zero address"
			);
		})

		it('can set new backend address', async () => {
			await LakeDiamondInstance.setBackendAddress(owner, { from: owner });
			await LakeDiamondInstance.setBackendAddress(backandAddress, { from: owner });
		})
	})

	
	describe('replace contracts by new version', function () {

		it('can set new instance of LakeDiamond', async () => {
			const tokensToSend = 200;
			await TokenInstance.transfer(LakeDiamondInstance.address, tokensToSend, { from: tokenOwner });
			const instanceBalance = await TokenInstance.balanceOf(LakeDiamondInstance.address);

			const newLakeDiamondInstance = await LakeDiamond.new(StorageInstance.address, owner, 100);
			
			StorageInstance.setLakeDiamondAddress(newLakeDiamondInstance.address);

			const lakeDiamondAddress = await StorageInstance.lakeDiamondAddress();
			assert.equal(newLakeDiamondInstance.address, lakeDiamondAddress);

			const newInstanceBalance = await TokenInstance.balanceOf(newLakeDiamondInstance.address);
			assert.equal(newInstanceBalance.toString(), instanceBalance.toString());
			
			StorageInstance.setLakeDiamondAddress(LakeDiamondInstance.address);
		})

		it('can set new instance of OrderStorageAdapter', async () => {
			const newOrderStorageAdapterInstance = await OrderStorageAdapter.new(StorageInstance.address);
			
			StorageInstance.setOrderStorageAdapterAddress(newOrderStorageAdapterInstance.address);

			const orderStorageAdapterAddress = await StorageInstance.orderStorageAdapterAddress();
			assert.equal(newOrderStorageAdapterInstance.address, orderStorageAdapterAddress);
			
			StorageInstance.setOrderStorageAdapterAddress(OrderStorageAdapterInstance.address);
		})

		it('can set new instance of ProposalStorageAdapter', async () => {
			const newProposalStorageAdapterInstance = await ProposalStorageAdapter.new(StorageInstance.address);
			
			StorageInstance.setProposalStorageAdapterAddress(newProposalStorageAdapterInstance.address);

			const proposalStorageAdapter = await StorageInstance.proposalStorageAdapterAddress();
			assert.equal(newProposalStorageAdapterInstance.address, proposalStorageAdapter);

			StorageInstance.setProposalStorageAdapterAddress(ProposalStorageAdapterInstance.address);
		})

		it(`cant call setLakeDiamondAndAdaptersAddresses from anyone except owner`, async () => {
			await catchRevert(
				StorageInstance.setLakeDiamondAndAdaptersAddresses(
					LakeDiamondInstance.address,
					ProposalStorageAdapterInstance.address,
					OrderStorageAdapterInstance.address,
					{ from: utils.getNotExistAddress(accounts, [storageOwner]) }
				),
			);
		})

		it(`cant call setLakeDiamondAndAdaptersAddresses with 0 addres in parameters`, async () => {
			await catchRevert(
				StorageInstance.setLakeDiamondAndAdaptersAddresses(
					utils.addressEmpty,
					ProposalStorageAdapterInstance.address,
					OrderStorageAdapterInstance.address,
					{ from: storageOwner }
				)
			);
			
			await catchRevert(
				StorageInstance.setLakeDiamondAndAdaptersAddresses(
					LakeDiamondInstance.address,
					utils.addressEmpty,
					OrderStorageAdapterInstance.address
				),
				{ from: storageOwner }
			);

			await catchRevert(
				StorageInstance.setLakeDiamondAndAdaptersAddresses(
					LakeDiamondInstance.address,
					ProposalStorageAdapterInstance.address,
					utils.addressEmpty,
					{ from: storageOwner }
				)
			);
			
			await catchRevert(
				StorageInstance.setLakeDiamondAndAdaptersAddresses(
					utils.addressEmpty,
					utils.addressEmpty,
					OrderStorageAdapterInstance.address,
					{ from: storageOwner }
				)
			);
			
			await catchRevert(
				StorageInstance.setLakeDiamondAndAdaptersAddresses(
					LakeDiamondInstance.address,
					utils.addressEmpty,
					utils.addressEmpty,
					{ from: storageOwner }
				)
			);
			
			await catchRevert(
				StorageInstance.setLakeDiamondAndAdaptersAddresses(
					utils.addressEmpty,
					ProposalStorageAdapterInstance.address,
					utils.addressEmpty,
					{ from: storageOwner }
				)
			);
			
			await catchRevert(
				StorageInstance.setLakeDiamondAndAdaptersAddresses(
					utils.addressEmpty,
					utils.addressEmpty,
					utils.addressEmpty,
					{ from: storageOwner }
				)
			);
		})


		it('can set new instance of LakeDiamond, ProposalStorageAdapter and OrderStorageAdapter by call setLakeDiamondAndAdaptersAddresses', async () => {
			const tokensToSend = 200;
			await TokenInstance.transfer(LakeDiamondInstance.address, tokensToSend, { from: tokenOwner });
			const instanceBalance = await TokenInstance.balanceOf(LakeDiamondInstance.address);

			const newLakeDiamondInstance = await LakeDiamond.new(StorageInstance.address, owner, 100);
			const newProposalStorageAdapterInstance = await ProposalStorageAdapter.new(StorageInstance.address);
			const newOrderStorageAdapterInstance = await OrderStorageAdapter.new(StorageInstance.address);
			
			StorageInstance.setLakeDiamondAndAdaptersAddresses(
				newLakeDiamondInstance.address,
				newProposalStorageAdapterInstance.address,
				newOrderStorageAdapterInstance.address
			);

			const lakeDiamondAddress = await StorageInstance.lakeDiamondAddress();
			assert.equal(newLakeDiamondInstance.address, lakeDiamondAddress);

			const proposalStorageAdapter = await StorageInstance.proposalStorageAdapterAddress();
			assert.equal(newProposalStorageAdapterInstance.address, proposalStorageAdapter);

			const orderStorageAdapterAddress = await StorageInstance.orderStorageAdapterAddress();
			assert.equal(newOrderStorageAdapterInstance.address, orderStorageAdapterAddress);

			const newInstanceBalance = await TokenInstance.balanceOf(newLakeDiamondInstance.address);
			assert.equal(newInstanceBalance.toString(), instanceBalance.toString());
			
			
			await StorageInstance.setLakeDiamondAndAdaptersAddresses(
				LakeDiamondInstance.address,
				ProposalStorageAdapterInstance.address,
				OrderStorageAdapterInstance.address
			);
		})

	})
})
