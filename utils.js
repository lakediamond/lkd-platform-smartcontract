let _ = require("lodash");
let Promise = require("bluebird");
const BN = require('bn.js');

module.exports = {
    addressEmpty: "0x0000000000000000000000000000000000000000",

    assertEvent: function(contract, filter) {
        return new Promise((resolve, reject) => {
            var event = contract[filter.event]();
            event.watch();
            event.get((error, logs) => {
                var log = _.filter(logs, filter);
                if (!_.isEmpty(log)) {
                    resolve(log);
                } else {
                    throw Error("Failed to find filtered event for " + filter.event);
                }
            });
            event.stopWatching();
        });
    },

    createProposalWithHint: async (LakeDiamondInstance, price, amount, afterId, sendConfig = {}) => {
        const proposalCreatingResult = await LakeDiamondInstance.createProposal(price, amount, afterId, sendConfig);

        return proposalCreatingResult.logs[0].args["id"];
    },

    createProposal: async (LakeDiamondInstance, price, amount, sendConfig = {}) => {
        const integetPrice = Math.round(price * 100);

        const proposalCreatingResult = await LakeDiamondInstance.createProposal(integetPrice, amount, 0, sendConfig);

        return proposalCreatingResult.logs.filter((item) => item.event == "ProposalCreated")[0].args["id"];
    },

    withdrawAllProposals: async () => {
        let proposalId = await ProposalStorageAdapterInstance.getHeadId();
        let i = 0; 
        while (proposalId.toString() != "0") {
            let proposal = await ProposalStorageAdapterInstance.getProposal(proposalId);
    
            console.log(
                `withdraw proposal id: ${proposalId.toString()}, price: ${proposal.price.toString()}, token amount: ${proposal.tokenAmount}`);

            LakeDiamondInstance.withdrawProposal(proposalId, { from: proposal.owner });
    
            proposalId = proposal.afterId.toString();
        }
    },

    createOrder: async (LakeDiamondInstance, typeId, metadata, price, tknAmount, exchangeRate, sendConfig = {}) => {
        const integerExchangeRate = Math.round(exchangeRate * 100);
        const integerPrice = Math.round(price * 100);

        sendConfig.value = new BN(10).pow(new BN(18)).mul(new BN(integerPrice)).mul(new BN(tknAmount)).div(new BN(integerExchangeRate));

        const proposalCreatingResult = 
            await LakeDiamondInstance.createOrder(typeId, metadata, integerPrice, tknAmount, integerExchangeRate, sendConfig);

        return proposalCreatingResult.logs[0].args["orderId"];
    },
    
    getNotExistAddress: (accounts, filterValues) => {
        const address = accounts.find((value) => { 
            return filterValues.findIndex((filterValue) => { return filterValue === value }) < 0 
        });
        
        return address;
    },

    getNetworkId: async (web3) => {
        return new Promise((resolve, reject) => {
            web3.eth.net.getId((error, networId) => {
                if (networId) {
                    resolve(networId);
                } else {
                    reject(error);
                }
            });
        });
    }
}
