## LKD platform smart contract

This repo contains the set of smart contracts which manage (in Ethereum):
- Proposals: users sending LKD in order to capture future orders
- Orders: Lakediamond creating an ETH order (coming from a diamond purchase from industrial clients)
- Matching order to proposals and dealing the rewards

More documentations in confluence:
- Smart contract [on-chain logic](https://lakediamond.atlassian.net/wiki/spaces/TOK/pages/1900545/On-chain+logic)
- [High level workflow diagram](https://lakediamond.atlassian.net/wiki/spaces/TOK/pages/17891331/High+level+workflow+diagram)